/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signutadupdater;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.nio.file.Paths;
import java.security.PrivilegedActionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;
import signutadupdater.ui.MainFrame;

public class SignUTADUpdater {
    
    private static final String statusInitializingUpdate = "É necessário atualizar a aplicação. Este é um procedimento automático.\nPor favor aguarde.";
    private static final String statusUpdated = "A aplicação foi atualizada com sucesso.";
    private static final String statusUpdatedManualInitialize = statusUpdated + "\nPor favor volte a tentar assinar no GesDoc.";
        
    private static final String jarURL = "https://gesdoc.utad.pt/Installers/Assinatura/Assinatura Digital UTAD.jar".replace(" ", "%20");
    private static final String jarNome = "Assinatura Digital UTAD.jar";
    
    private static String thisVersion = "";
    private static String requiredVersion = "";
    
    private static String jarPathArg = "";
    
    public static void main(String[] args) 
    {
        try
        {
            thisVersion = args[0];
            requiredVersion = args[1];
            
                
            String thisPath = URLDecoder.decode(SignUTADUpdater.class.getProtectionDomain().getCodeSource().getLocation().getFile(), "UTF-8");
            thisPath = new File(thisPath).getParentFile().getPath() + File.separator;
            String jarPath = thisPath + jarNome;

            jarPath = jarPath.replace('/', File.separatorChar);

            MainFrame fr = new MainFrame();
            fr.setVisible(true);
            
            fr.SetStatus(statusInitializingUpdate + "\nAtual: v" + thisVersion + "\nNecessária: v" + requiredVersion);
                    
            byte[] jar = GetJar();
            
            FileOutputStream fos = new FileOutputStream(jarPath);
            fos.write(jar);
            fos.close();
            
            if(args.length > 2)
            {
                fr.SetStatus(statusUpdated);
                
                ProcessBuilder proc = new ProcessBuilder("java", "-jar", jarNome, args[2]);
                proc.directory(new File(thisPath));
                
                proc.start();
            }
            else
            {
                fr.SetStatus(statusUpdatedManualInitialize);
            }
        }
        catch (MalformedURLException ex) 
        {
            
        }
        catch (IOException ex)
        {
            
        }
        catch (RuntimeException ex) 
        {
            
        }
        finally
        {
            System.exit(0);
        }
    }
    
    
    
    //<editor-fold desc="Download" defaultstate="collapsed">
    
    private static byte[] GetJar() throws IOException, MalformedURLException, RuntimeException
    {
        return DownloadFile(jarURL, "");
    }
    
    public static final HttpURLConnection openDownloadConnection(URL url, String wsAuth) throws IOException{
        HttpURLConnection con = (HttpURLConnection) url.openConnection ();
        con.setDoInput(true);
        con.setDoOutput (false);
        
        con.setRequestMethod("GET");

        //con.setRequestProperty("Accept", "application/octet-stream");
        
        if(wsAuth != null && wsAuth != "")
        {
            if(!wsAuth.contains(" ")) wsAuth = "Basic " + wsAuth;
            con.setRequestProperty("Authorization", wsAuth);
        }
        
        return con;
    }
    private static byte[] DownloadFile(String fullUrl, String wsAuth) throws MalformedURLException, IOException, RuntimeException
    {
        URLConnection c = null;
        try
        {
            URL requestAddress = new URL(fullUrl);

            HttpURLConnection conn = openDownloadConnection(requestAddress, wsAuth);
            c = conn;

            int respcode = conn.getResponseCode();

            if (respcode > 299) 
            {
                throw new RuntimeException("Failed : HTTP error code : " + respcode);
            }

            InputStream in = conn.getInputStream(); 
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            byte[] b = new byte[1024];
            int count;

            while ((count = in.read(b)) > 0)
            {
                out.write(b, 0, count);
            }
            
            out.close();
            conn.disconnect();
            
            return out.toByteArray();
        }
        catch(Throwable T)
        {
            if (c != null && c.getClass() == HttpURLConnection.class)
            {
                ((HttpURLConnection)c).disconnect();
            }
            throw T;
        }
    }
    
    //</editor-fold>
    
    //<editor-fold desc="Erros" defaultstate="collapsed">
    /*
    private static Boolean LogError(String link, Throwable T)
    {
        Boolean ret = false;
        
        try
        {
            String postUrl = url + "/logerro";
            URL obj = new URL(postUrl);
            if(postUrl.startsWith("https://"))
            {
                HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

                //add reuqest header
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type", "application/json");

                // Send post request
                ExceptionHandler handler = new ExceptionHandler(idProcesso, login, "Assinatura", link, T);

                String gson = handler.ToJson();
                
                gson = ToISO88591(gson);
                
                con.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.writeBytes(gson);
                wr.flush();
                wr.close();

                int responseCode = con.getResponseCode();
                ret = responseCode < 300;

                return ret;
            }
            else
            {
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                //add reuqest header
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type", "application/json");

                // Send post request
                ExceptionHandler handler = new ExceptionHandler(idProcesso, login, "Assinatura", link, T);

                String gson = handler.ToJson();

                con.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.writeBytes(gson);
                wr.flush();
                wr.close();

                int responseCode = con.getResponseCode();
                ret = responseCode < 300;


                return ret;
            }
        }
        catch(Throwable T1)
        {
            return false;
        }
    }
    */
    //</editor-fold>
}
