# README #

### O que é necessário para assinar? ###

* É necessário fazer um JSON com os seguintes parâmetros:  
**SPLib**: Biblioteca onde está o PDF a ser assinado, assim como a biblioteca para onde vai  
**Processo**: Nome do processo onde está o ficheiro  
**Filename**: Nome do ficheiro  
**WSUrl**: URL do IGesDoc, usado para uploads e downloads  
**WSToken**: Token para assinatura  
**DownloadUrl**: Link completo para o download do PDF  
**UploadUrl**: Link completo para o upload do PDF.  
***Nota***: Os parâmetros **SPLib**, **Processo**, **Filename**, **WSUrl** e **WSToken** são ignorados para o download caso seja dado o parâmetro **DownloadUrl**, e para o upload caso seja dado o **UploadUrl**.  
**WSAuth**: Autenticação para o web service ([MÉTODO] [USER]:[PW] em Base64. Basic no caso do IGesDoc)  
**SignatureField**: Nome do campo de assinatura a assinar. Neste momento está a ser ignorado, e a ser usado o primeiro campo existente. Caso não exista nenhum campo de assinatura, faz invisível.  
**urX**: Coordenada X do canto superior direito  
**urY**: Coordenada Y do canto superior direito  
**llX**: Coordenada X do canto inferior esquerdo  
**llY**: Coordenada Y do canto inferior esquerdo  
**ReasonCaption**: Legenda da razão da assinatura (Ex: "Motivo", "Despacho")  
**Reason**: Texto da razão da assinatura  
**ID**: ID do processo  
**Login**: Nome de utilizador do assinante  
  
* De seguida, este JSON deve ser codificado em Base64, e deve ser fornecido ao utilizador um link no seguinte formato:  
**utadsign://[DOMINIO_DO_SITE]/[JSON_B64]**  
Por exemplo: utadsign://gesdoc.utad.pt/eyJTUExpYiI6IlByb2Nlc...9naW4iOiJlanVzdGlubyJ9==

* Ao seguir esse link, é apresentada ao utilizador a interface gráfica da aplicação, sendo só necessário introduzir o seu pin de assinatura.

### Como instalar? ###

O repositório tem uma pasta Installers, onde estão os instaladores para Linux, macOS e Windows.
O instalador para Linux é um zip na pasta linux. O instalador para macOS é um dmg na pasta mac e o instalador para Windows é um exe na pasta Installers/windows/SignUTADInstaller/Package/Release.
A aplicação necessita de ter o JRE instalado, de preferência acima da versão 1.8.0 u121.

As instruções de instalação são semelhantes a qualquer outro programa:  
**Linux**: Correr o ficheiro install.sh - *sh install.sh*  
**macOS**: Duplo click no ficheiro Assinatura Digital UTAD.dmg, abrir a drive montada e arrastar o ficheiro Assinatura Digital UTAD para a pasta Aplicações à direita. As instruções estão no ecrã.  
**Windows**: Correr o ficheiro Assinatura Digital UTAD.exe, next next finnish.  

Se for necessário instalar o JRE, o link é **https://java.com/en/download/**  

### Contribuir ###

* Não fazer qualquer commit para o branch principal sem primeiro consultar Cláudio Pereira, Jorge Borges ou António Capela para análise.

### Com quem falar? ###

* A aplicação está sob responsabilidade de Cláudio Pereira (cpereira@utad.pt) e António Capela, ao abrigo dos Serviços de Informação e Comunicações da Universidade de Trás-os-Montes e Alto Douro.
* Cláudio Pereira e António Capela estão sob supervisão de Jorge Borges