/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.utad.application.desktopsign.ui;

/**
 *
 * @author claudio
 */
public interface IFrameAssinatura
{
    public void SetAllowTimerMessageUpdates(boolean allow);
    public void ShowMessage(String message, String caption);
    public void StartSignatureMessageTimer(String message, int seconds);
    public void StopSignatureMessageTimer();
    public void StartDebugTimerSignature();
}
