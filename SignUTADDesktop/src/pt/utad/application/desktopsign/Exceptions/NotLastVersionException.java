/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.utad.application.desktopsign.Exceptions;

/**
 *
 * @author claudio
 */
public class NotLastVersionException extends Exception 
{
    private String thisV = "";
    private String reqV = "";
    private String uname = "";

    /**
     * @return the thisV
     */
    public String getThisVersion() {
        return thisV;
    }

    /**
     * @return the reqV
     */
    public String getRequiredVersion() {
        return reqV;
    }
    
    public String getUsername() {return uname;}
    
    public NotLastVersionException(String thisVersion, String requiredVersion)
    {
        this(thisVersion, requiredVersion, "");
    }
    
    public NotLastVersionException(String thisVersion, String requiredVersion, String username)
    {
        super("Atualizada a aplicação do utilizador " + username + ". " + thisVersion + " para " + requiredVersion);
        
        uname = username;
        thisV = thisVersion;
        reqV = requiredVersion;
    }
}
