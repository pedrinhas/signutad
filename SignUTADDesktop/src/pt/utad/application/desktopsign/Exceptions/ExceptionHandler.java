/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.utad.application.desktopsign.Exceptions;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.PrintWriter;
import java.io.StringWriter;
/*

public class ErroJsonHandler
    {
        [DataMember]
        public string idProcesso { get; set; }
        [DataMember]
        public string login { get; set; }
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public string acao { get; set; }
        [DataMember]
        public string link { get; set; }
        [DataMember]
        public string stackTrace { get; set; }
    }

*/
public class ExceptionHandler {

    private String idProcesso;
    public String getIdProcesso() {
        return idProcesso;
    }
    public void setIdProcesso(String idProcesso) {
        this.idProcesso = idProcesso;
    }
    
    private String login;
    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    
    private String message;
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    private String acao;
    public String getAcao() {
        return acao;
    }
    public void setAcao(String acao) {
        this.acao = acao;
    }

    private String link;
    public String getLink() {
        return link;
    }
    public void setLink(String link) {
        this.link = link;
    }
    
    private String stackTrace;
    public String getStackTrace() {
        return stackTrace;
    }
    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }
    
    public ExceptionHandler(String rjson)
    {
        Gson g = new Gson();
        
        ExceptionHandler novo = g.fromJson(rjson, ExceptionHandler.class);
        
        idProcesso = novo.idProcesso;
        login = novo.login;
        message = novo.message;
        acao = novo.acao;
        link = novo.link;
        stackTrace = novo.stackTrace;
    }
    public ExceptionHandler(String _idProcesso, String _login, String _acao, String _link, Throwable T)
    {
        idProcesso = _idProcesso;
        login = _login;
        acao = _acao;
        link = _link;
        
        
        message = T.getClass().toString() + ": " + T.getMessage();

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        T.printStackTrace(pw);

        stackTrace = sw.toString();
    }
    
    public String ToJson()
    {
        Gson g = new GsonBuilder().disableHtmlEscaping().create();
        
        return g.toJson(this);
    }
}