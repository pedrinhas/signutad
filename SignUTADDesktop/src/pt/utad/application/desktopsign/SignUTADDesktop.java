/*
 *
 * @author cpereira
 * 
 */

package pt.utad.application.desktopsign;

import pt.utad.application.desktopsign.Exceptions.PinLockedException;
import pt.utad.application.desktopsign.Exceptions.ExceptionHandler;
import pt.utad.application.desktopsign.Exceptions.CertificateRevokedException;
import pt.utad.application.desktopsign.Exceptions.NotLastVersionException;
import pt.utad.application.desktopsign.Exceptions.NoSignatureFieldsException;
import pt.utad.application.desktopsign.Exceptions.InvalidOSException;
import pt.utad.application.desktopsign.Exceptions.CertificateChainException;
import pt.utad.application.desktopsign.Tools.ArgumentHandler;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.security.CrlClientOnline;
import com.itextpdf.text.pdf.security.ExternalDigest;
import com.itextpdf.text.pdf.security.ExternalSignature;
import com.itextpdf.text.pdf.security.LtvVerification;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.OcspClient;
import com.itextpdf.text.pdf.security.OcspClientBouncyCastle;
import com.itextpdf.text.pdf.security.PdfPKCS7;
import com.itextpdf.text.pdf.security.PrivateKeySignature;
import com.itextpdf.text.pdf.security.ProviderDigest;
import com.itextpdf.text.pdf.security.TSAClient;
import java.util.Base64;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Console;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PrivilegedActionException;
import java.security.Security;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;
import org.apache.commons.io.IOUtils;
import org.poreid.CardFactory;
import org.poreid.CardNotPresentException;
import org.poreid.CardTerminalNotPresentException;
import org.poreid.POReIDException;
import org.poreid.Pin;
import org.poreid.PkAlias;
import org.poreid.UnknownCardException;
import org.poreid.cc.CCConfig;
import org.poreid.cc.CardSpecificReferences;
import org.poreid.cc.CitizenCard;
import org.poreid.config.POReIDConfig;
import org.poreid.crypto.POReIDProvider;
import org.poreid.dialogs.pindialogs.PinBlockedException;
import org.poreid.dialogs.pindialogs.PinEntryCancelledException;
import org.poreid.dialogs.pindialogs.PinTimeoutException;
import org.poreid.dialogs.pindialogs.verifypin.VerifyPinDialogController;
import org.poreid.dialogs.selectcard.CanceledSelectionException;
import pt.utad.application.desktopsign.Exceptions.BadArgumentException;
import pt.utad.application.desktopsign.Tools.MultiSignatureArgumentHandler;
import pt.utad.application.desktopsign.Tools.TSAClientSignUTADDesktop;
import pt.utad.application.desktopsign.ui.IFrameAssinatura;
import pt.utad.application.desktopsign.ui.MainFrame;
import pt.utad.application.desktopsign.ui.MainFrameMulti;

public class SignUTADDesktop {

    //<editor-fold desc="var" defaultstate="collapsed">
    private static String url;
    
    private static String reasonCaption;
    private static String reason;
    
    static Boolean visibleSignature;
    static String signedProcessSuffix = "_assinado";
    static String signatureField;
    static float urx, ury, llx, lly;
    
    private static final String applicationName = "Assinatura Digital UTAD";
    
    private static final String captionError = "Erro";
    private static final String captionInfo = "Informação";
    private static final String captionSuccess = "Sucesso";
    
    private static final String statusInitializing = "A inicializar a aplicação";
    private static final String statusDownloading = "A fazer o download";
    private static final String statusDownloaded = "Download concluído";
    
    private static final String statusSigningPrep = "A preparar a assinatura";
    private static final String statusSigning = "A assinar";
    private static final String statusSigned = "Documento assinado com sucesso";
    
    private static final String statusLTVBegin = "A adicionar dados de verificação";
    private static final String statusLTVEnd = "Dados de verificação adicionados";
    
    private static final String statusUploading = "A fazer o upload";
    private static final String statusUploaded = "Upload concluído";
    
    private static final String statusLacrar = "A lacrar";
    private static final String statusLacrado = "Lacrado";
    
    private static final String statusSuccess = "O documento foi assinado com sucesso. Pode fechar a informação no GesDoc.";
    private static final String statusSuccessMulti = "Todos os documentos foram assinados com sucesso.";
    
    private static final String statusTimerNoOCSP = "A assinatura está a demorar mais do que o previsto\nProvavelmente é uma falha no serviço de verificação do certificado.\nPode fechar a janela e tentar mais tarde.";
    
    private static final String errorGeneral = "Não foi possível assinar o documento. Por favor contacte o apoio do GesDoc.";
    private static final String errorCheckCard = "Verifique se tem o leitor ligado, e se o cartão está presente.";
    private static final String errorSignature = "Não foi possível assinar o documento, por favor tente de novo. Se o problema persistir, contacte o apoio do GesDoc.";
    private static final String errorGeneralException = "Não foi possível assinar o documento. Por favor contacte o apoio do GesDoc. A excepção é ";
    
    private static final String errorBadArguments = "Não foi possível assinar o documento. Por favor contacte o apoio do GesDoc. O código de erro é -1";
    private static final String errorCanceled = "Cancelou a assinatura, a sair.";
    private static final String errorSignTimeout = "A assinatura foi cancelada por inatividade. Por favor tente de novo.";
    private static final String errorWSNotFound = "O serviço está temporariamente indisponível. Por favor tente mais tarde.";
    private static final String errorNoSignatureFields = "Não existem campos de assinatura disponíveis.";
    private static final String errorLockedPin = "O PIN de assinatura está bloqueado. Por favor dirija-se a uma Loja do Cidadão.";
    private static final String errorInvalidOS = "O sistema operativo que está a utilizar não é suportado. A Aplicação de Assinatura apenas suporta Windows, macOS e Linux.";
    private static final String errorCertRevoked = "O certificado de assinatura encontra-se revogado. Por favor dirija-se a uma Loja do Cidadão.";
    private static final String errorNoUpdateJar = "Não foi possível encontrar o ficheiro para atualizar. Por favor faça download do instalador no GesDoc.";
    
    private static MainFrame frame;
    private static MainFrameMulti multiFrame;
    
    private static final String updaterJarName = "updater.jar";
 
    //</editor-fold>

    private static Boolean debug = false;
    
    private static final String Version = "4.0.6";    
    private static final String VersionString = String.format("%s%s", Version, debug ? " debug" : "");
    
    private static final String applicationTitle = String.format("%s (v%s)", applicationName, VersionString);
    
    public static void main(String[] args) throws InterruptedException
    {
        if (Arrays.asList(args).contains("-v") || Arrays.asList(args).contains("--version")) 
        {
            System.out.println(VersionString);
        }
        else if(debug)
        {
            DebugSignature();
            //DebugSignatureMulti();
            //TesteUploads(args);
        }
        else
        {
            ProdSignature(args);
        }
    }
    
    //<editor-fold defaultstate="collapsed" desc="Assinatura">
    
    //<editor-fold desc="Produção" defaultstate="collapsed">
    
    private static void ProdSignature(String[] args)
    {
        try
        {            
            //DA PARA QUALQUER ENDEREÇO
            String argument = args[0].substring(args[0].indexOf("/", 13) + 1);
            byte[] argDecoded = Base64.getDecoder().decode(argument);
            String json = "";

            if (argDecoded == null) argDecoded = Base64.getDecoder().decode(argument.substring(0, argument.length() - 1));

            if (argDecoded != null) 
            {
                json = new String(argDecoded);
                ArgumentHandler arg = new ArgumentHandler(json);

                url = arg.getWSUrl();
                if(!IsLastVersion())
                {
                    throw new NotLastVersionException(Version, lastVersionFromWS, arg.getLogin());
                }
                        
                try
                {
                    if(arg.getMultiple())
                    {
                        //<editor-fold desc="Multi" defaultstate="collapsed">                        
                        multiFrame = new MainFrameMulti(statusInitializing, statusInitializing);
                        multiFrame.setTitle(applicationTitle);
                        multiFrame.setVisible(true);
                        
                        String multiSignJSON = getTextFromURL(arg.getMultipleUrlFileList());
                        MultiSignatureArgumentHandler multiArg = new MultiSignatureArgumentHandler(multiSignJSON);

                        HashMap<String, String> links = new HashMap<String, String>();
                        for(String ref : multiArg.getFileRefs())
                        {
                            String ulLink = multiArg.getBaseURLUpload() + "?token=" + arg.getMultipleToken() + "&ref=" + ref;
                            String dlLink = multiArg.getBaseURLDownload() + "?token=" + arg.getMultipleToken() + "&ref=" + ref;

                            links.put(dlLink, ulLink);
                        }

                        SignPDF_Multi(links);
                        
                        multiFrame.ShowMessage(statusSuccessMulti, captionSuccess);
                        //</editor-fold>
                    }   
                    else
                    {
                        //<editor-fold desc="Single" defaultstate="collapsed">
                        frame = new MainFrame(statusInitializing);
                        frame.setTitle(applicationTitle);
                        frame.setVisible(true);
                        
                        signatureField = arg.getSignatureField();
                        reason = arg.getReason();
                        reasonCaption = arg.getReasonCaption();

                        visibleSignature = arg.getUrX() != null && arg.getUrY() != null && arg.getLlX() != null && arg.getLlY() != null;
                        if(visibleSignature)
                        {
                            urx = arg.getUrX();
                            ury = arg.getUrY();
                            llx = arg.getLlX();
                            lly = arg.getLlY();
                        }

                        //<editor-fold defaultstate="expanded" desc="Assinar">

                        byte[] doc = null;
                        for (int i = 0; i < 3 && (doc == null || doc.length == 0); i++) {
                            try
                            {
                                //doc = DownloadFile(url, arg.getSPLib(), arg.getProcesso(), arg.getFilename(), arg.getWSToken(), arg.getWSAuth());
                                doc = DownloadFile(arg.getMultiple(), arg.getDownloadUrl(), arg.getWSAuth());
                            }
                            catch(RuntimeException ex)
                            {
                                LogError(arg.getID(), arg.getLogin(), args[0], ex);
                            }
                        }

                        if (doc == null || doc.length == 0)
                        {
                            frame.ShowMessage(errorWSNotFound, captionError);

                            LogError(arg.getID(), arg.getLogin(), argument, new Exception("Não foi possível chegar ao WS, ou fazer o download do ficheiro"));
                            System.exit(0);
                        }

                        WriteToConsole("Downloaded file " + arg.getFilename());
                        WriteToConsole("Size: " + doc.length / 1024 + "kb");

                        //30 SEGUNDOS DE TIMEOUT
                        byte[] signedDoc = null;

                        signedDoc = SignPDF(doc);

                        WriteToConsole("Making LTV");
                        signedDoc = MakeLTV(arg.getMultiple(), signedDoc);
                        WriteToConsole("LTV done");

                        doc = null;
                        argDecoded = null;
                        
                        //WriteToConsole("Size: " + signedDoc.length / 1024 + "kb");

                        String signedDocName = "";
                        if(arg.getFilename() != null && arg.getFilename() != "")
                        {
                           signedDocName = arg.getFilename().substring(0, arg.getFilename().lastIndexOf('.')) + signedProcessSuffix + arg.getFilename().substring(arg.getFilename().lastIndexOf('.')); 
                        }
                        
                        boolean uploaded = false;
                        for (int i = 0; i < 3 && !uploaded; i++) {
                            try
                            {
                                //uploaded = UploadFile(url, arg.getSPLib(), arg.getProcesso(),signedDocName, signedDoc, arg.getWSToken(), arg.getWSAuth());
                                uploaded = UploadFile(arg.getMultiple(), arg.getUploadUrl(), signedDocName, arg.getWSAuth(), signedDoc);
                                if(!uploaded)
                                {
                                    frame.ShowMessage(errorWSNotFound, captionError);
                                    System.exit(0);
                                }
                            }
                            catch(RuntimeException ex)
                            {
                                LogError(arg.getID(), arg.getLogin(), args[0], ex);
                            }
                        }
                        if (!uploaded)
                        {
                            LogError(arg.getID(), arg.getLogin(), args[0], new Exception("Não foi possível chegar ao WS, ou fazer o upload do ficheiro"));
                            frame.ShowMessage(errorWSNotFound, captionError);
                            System.exit(0);
                        }

                        signedDoc = null;

                        frame.SetStatus(statusSuccess);

                        Thread.sleep(2000);

                        //</editor-fold>
                        
                        //</editor-fold>
                    }
                }
                catch(Throwable T)
                {
                    HandleException(T, arg, args);
                }
            }
            else 
            {
                throw new BadArgumentException();
            }
        }
        catch(Throwable T)
        {
            HandleException(T, args);
        }
        
        System.exit(0);
    }
    
    private static byte[] SignPDF(byte[] srcBytes) throws IOException, DocumentException, GeneralSecurityException, InvalidOSException, CertificateExpiredException, CertificateNotYetValidException, CertificateRevokedException, KeyStoreException, NoSuchAlgorithmException, UnrecoverableKeyException, PrivilegedActionException, PinLockedException, CertificateChainException, NoSignatureFieldsException
    {
        frame.SetStatus(statusSigningPrep, true);
        frame.SetProgressBarMax(100);
        
        ByteArrayInputStream src = new ByteArrayInputStream(srcBytes);
        
        PrivateKey pk = null;
        Certificate[] chain = null;
                
        KeyStore ks = null;

        String assinaturaCertifLabel = "";
        String provider = "";

        // reader and stamper
        PdfReader reader = new PdfReader(src);
        WriteToConsole("PdfReader built");
        
        AcroFields acro = reader.getAcroFields();
        
        //ESTA LINHA FAZ COM QUE O signatureField DO JSON SEJA IGNORADO.
        signatureField = GetSignatureFieldName(acro);
        
        ByteArrayOutputStream dest = new ByteArrayOutputStream();
        PdfStamper stamper = PdfStamper.createSignature(reader, dest, '\0', null, true);
        WriteToConsole("PdfStamper built");

        // appearance
        PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
        WriteToConsole("PdfSignatureAppearance built");
        
        if(reason != null)
        {
            if(reasonCaption != null) appearance.setReasonCaption(reasonCaption);
            else appearance.setReasonCaption("Motivo: ");
            appearance.setReason(reason);
            
            appearance.setLayer4Text(reasonCaption + reason);
        }
        
        frame.AdvanceProgress(25);
        
        //<editor-fold desc="Tratar do signatureField e do visibleSignature" defaultstate="Collapsed">
        
        if(signatureField != null && !signatureField.isEmpty())
        {
            appearance.setVisibleSignature(signatureField);
            
            WriteToConsole("Signing on field " + signatureField);
        }
        else if(visibleSignature != null && visibleSignature) 
        {
            AcroFields acros = reader.getAcroFields();
            String sigFieldName = "sig" + SignatureCount(acros);
            
            appearance.setVisibleSignature(new Rectangle(llx, lly, urx, ury), 1, sigFieldName);
            
            WriteToConsole("Signing with visible signature on urx: " + urx + "; ury: " + ury + "; llx: " + llx + "; lly: " + lly);
        }
        
        //</editor-fold>
        
        frame.AdvanceProgress(25);
        
        appearance.setSignatureCreator("Serviços de Informática e Comunicações - UTAD");
        
        //Timestamp
        // mvale - Instancia um cliente TSA customizado, que suporta o uso do servidor TSA da ANO caso o servidor TSA do Cartão de Cidadão falhe (quer por falta de timestamps, quer por outras razões)
        // O código deste cliente TSA foi copiado do código-fonte do IText e modificado
        // Esse código-fonte encontra-se disponível aqui: https://github.com/sunrenjie/itext/blob/master/itext/src/main/java/com/itextpdf/text/pdf/security/TSAClientBouncyCastle.java
        TSAClient tsa = new TSAClientSignUTADDesktop("http://ts.cartaodecidadao.pt/tsa/server");
        
        try{
            tsa.getTimeStampToken(new byte[] { 0x00 });
        }
        catch(Throwable T){
            
        }
        WriteToConsole("Built TSA client");

        //OCSP
        OcspClient ocsp = new OcspClientBouncyCastle();
        WriteToConsole("Built OCSP client");
        //CRL
        //List<CrlClient> crlList = new ArrayList<CrlClient>();
        //crlList.add(new CrlClientOnline(chain));

        frame.AdvanceProgress(25);
        frame.SetStatus(statusSigning);
        
        try
        {
            if(Security.getProvider(POReIDConfig.POREID) == null) Security.addProvider(new POReIDProvider());
            ks = KeyStore.getInstance(POReIDConfig.POREID);

            assinaturaCertifLabel = POReIDConfig.ASSINATURA;
            provider = POReIDConfig.POREID;      

            ks.load(null, null);
            WriteToConsole("Keystore loaded");

            pk = (PrivateKey)ks.getKey(assinaturaCertifLabel, null);
            chain = ks.getCertificateChain(assinaturaCertifLabel);

            if(chain == null)
            {        
                Security.removeProvider(POReIDConfig.POREID);
                Security.addProvider(new POReIDProvider());

                ks = KeyStore.getInstance(POReIDConfig.POREID);
                ks.load(null,null);

                pk = (PrivateKey)ks.getKey(POReIDConfig.ASSINATURA, null);
                chain = ks.getCertificateChain(POReIDConfig.ASSINATURA);

                if(chain == null)
                {
                    throw new CertificateChainException();
                }
            }

            // digital signature
            ExternalSignature es = new PrivateKeySignature(pk, "SHA-256", provider);
            ExternalDigest digest = new ProviderDigest(null);
        
            frame.AdvanceProgress(25);
            WriteToConsole("Private key and chain acquired");
            
            WriteToConsole("Signing");
            frame.StartSignatureMessageTimer(statusTimerNoOCSP, 31);
                
            MakeSignature.signDetached(appearance, digest, es, chain, null, ocsp, tsa, 0, MakeSignature.CryptoStandard.CADES);

            frame.StopSignatureMessageTimer();
            
            src.close();
            
            stamper.close();
            reader.close();
            WriteToConsole("Signed");
            frame.SetStatus(statusSigned, true);
            frame.SetProgressBarMax(100);
            
            return dest.toByteArray();
        }
        catch(Throwable t) {  
            throw t;
        }
    }
    
    private static void SignPDF_Multi(Map<String,String> links) throws IOException, DocumentException, GeneralSecurityException, InvalidOSException, CertificateExpiredException, CertificateNotYetValidException, CertificateRevokedException, KeyStoreException, NoSuchAlgorithmException, UnrecoverableKeyException, PrivilegedActionException, PinLockedException, CertificateChainException, NoSignatureFieldsException, PinBlockedException, Throwable
    {
        try
        {                    
            multiFrame.SetFileStatus(statusSigningPrep, true);
            multiFrame.SetFileProgressBarMax(100);
            
            PrivateKey pk = null;
            Certificate[] chain = null;

            KeyStore ks = null;

            String assinaturaCertifLabel = "";
            String provider = "";

            boolean pinOK = false;
            char[] pin = null;
            
            multiFrame.SetTotalProgressBarMax(links.keySet().size());
            
            int signed = 0;
            int total = links.keySet().size();
            
            long elapsed = 0;
            
            for(String dlLink : links.keySet())
            {         
                long start = System.currentTimeMillis();
                
                String plural = signed == 1 ? "" : "s";
                String pluralTotal = total == 1 ? "" : "s";
                
                String mainStatus = "Assinado" + plural + " " + signed + " de " + total + " ficheiro" + pluralTotal;
                
                if(signed > 1)
                {                    
                    long mediaTempos = elapsed / signed;
                    
                    long eta = mediaTempos * (links.size() - signed);

                    long second = (eta / 1000) % 60;
                    long minute = (eta / (1000 * 60)) % 60;
                    long hour = (eta / (1000 * 60 * 60)) % 24;

                    String time = String.format("%02d:%02d:%02d", hour, minute, second);
                    
                    String etaString  = "\nTempo restante: " + time;
                    
                    mainStatus = mainStatus + etaString;
                }
                
                multiFrame.SetTotalStatus(mainStatus);
                
                byte[] out = null;
                byte[] srcBytes = DownloadFile(true, dlLink, "");
                try
                {
                    multiFrame.SetFileProgressBarMax(100);
                    multiFrame.SetFileStatus(statusSigningPrep, true);
                    multiFrame.AdvanceFileProgress(10);
                    
                    ByteArrayInputStream src = new ByteArrayInputStream(srcBytes);
                    
                    // reader and stamper
                    PdfReader reader = new PdfReader(src);
                    WriteToConsole("PdfReader built");

                    AcroFields acro = reader.getAcroFields();

                    //ESTA LINHA FAZ COM QUE O signatureField DO JSON SEJA IGNORADO.
                    signatureField = GetSignatureFieldName(acro);

                    ByteArrayOutputStream dest = new ByteArrayOutputStream();
                    PdfStamper stamper = PdfStamper.createSignature(reader, dest, '\0', null, true);
                    WriteToConsole("PdfStamper built");

                    // appearance
                    PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
                    WriteToConsole("PdfSignatureAppearance built");

                    if(reason != null)
                    {
                        if(reasonCaption != null) appearance.setReasonCaption(reasonCaption);
                        else appearance.setReasonCaption("Motivo: ");
                        appearance.setReason(reason);

                        appearance.setLayer4Text(reasonCaption + reason);
                    }

                    multiFrame.AdvanceFileProgress(20);
                    
                    //<editor-fold desc="Tratar do signatureField e do visibleSignature" defaultstate="Collapsed">

                    if(signatureField != null && !signatureField.isEmpty())
                    {
                        appearance.setVisibleSignature(signatureField);

                        WriteToConsole("Signing on field " + signatureField);
                    }
                    else if(visibleSignature != null && visibleSignature) 
                    {
                        AcroFields acros = reader.getAcroFields();
                        String sigFieldName = "sig" + SignatureCount(acros);

                        appearance.setVisibleSignature(new Rectangle(llx, lly, urx, ury), 1, sigFieldName);

                        WriteToConsole("Signing with visible signature on urx: " + urx + "; ury: " + ury + "; llx: " + llx + "; lly: " + lly);
                    }

                    multiFrame.AdvanceFileProgress(20);

                    //</editor-fold>

                    appearance.setSignatureCreator("Serviços de Informática e Comunicações - UTAD");

                    //Timestamp
                    // mvale - Instancia um cliente TSA customizado, que suporta o uso do servidor TSA da ANO caso o servidor TSA do Cartão de Cidadão falhe (quer por falta de timestamps, quer por outras razões)
                    // O código deste cliente TSA foi copiado do código-fonte do IText e modificado
                    // Esse código-fonte encontra-se disponível aqui: https://github.com/sunrenjie/itext/blob/master/itext/src/main/java/com/itextpdf/text/pdf/security/TSAClientBouncyCastle.java
                    TSAClient tsa = new TSAClientSignUTADDesktop("http://ts.cartaodecidadao.pt/tsa/server");
                    WriteToConsole("Built TSA client");
                    
                    //OCSP
                    OcspClient ocsp = new OcspClientBouncyCastle();
                    WriteToConsole("Built OCSP client");
                    //CRL
                    //List<CrlClient> crlList = new ArrayList<CrlClient>();
                    //crlList.add(new CrlClientOnline(chain));
                    
                    multiFrame.AdvanceFileProgress(20);
                    multiFrame.SetFileStatus(statusSigning);
                        
                    if(!pinOK)
                    {
                        if(Security.getProvider(POReIDConfig.POREID) == null) Security.addProvider(new POReIDProvider());
                        ks = KeyStore.getInstance(POReIDConfig.POREID);

                        assinaturaCertifLabel = POReIDConfig.ASSINATURA;
                        provider = POReIDConfig.POREID;
                        
                        do
                        {
                            CitizenCard cc = CardFactory.getCard();
                            CardSpecificReferences csr = cc.getCardSpecificReferences();
                            Pin p = csr.getCryptoReferences(PkAlias.ASSINATURA);

                            VerifyPinDialogController controller = VerifyPinDialogController.getInstance(CCConfig.TIMEOUT, p, csr.getLocale());
                            byte[] pinBytes = controller.askForPin();
                            pin = new String(pinBytes).toCharArray();
                            pinOK = cc.verifyPin(p, pinBytes);
                            
                            //Se cancelar lança uma exceção, e se o bloquear também.
                        } while(!pinOK);
                        
                        
                        ks.load(null, null);
                        WriteToConsole("Keystore loaded");
                        
                        pk = (PrivateKey)ks.getKey(assinaturaCertifLabel, pin);

                        chain = ks.getCertificateChain(assinaturaCertifLabel);

                        if(chain == null)
                        {        
                            Security.removeProvider(POReIDConfig.POREID);
                            Security.addProvider(new POReIDProvider());

                            ks = KeyStore.getInstance(POReIDConfig.POREID);
                            ks.load(null,null);

                            pk = (PrivateKey)ks.getKey(POReIDConfig.ASSINATURA, null);
                            chain = ks.getCertificateChain(POReIDConfig.ASSINATURA);

                            if(chain == null)
                            {
                                throw new CertificateChainException();
                            }
                        }
                    }
                    
                    // digital signature
                    ExternalSignature es = new PrivateKeySignature(pk, "SHA-256", provider);
                    ExternalDigest digest = new ProviderDigest(null);
                    
                    WriteToConsole("Private key and chain acquired");

                    WriteToConsole("Signing");
                    multiFrame.StartSignatureMessageTimer(statusTimerNoOCSP, 31);
                    multiFrame.AdvanceFileProgress(10);
                    
                    MakeSignature.signDetached(appearance, digest, es, chain, null, ocsp, tsa, 0, MakeSignature.CryptoStandard.CADES);
                    
                    multiFrame.SetFileProgress(100);
                    multiFrame.SetFileStatus(statusSigned);

                    multiFrame.StopSignatureMessageTimer();

                    src.close();

                    stamper.close();
                    reader.close();
                    WriteToConsole("Signed");
                    
                    out = MakeLTV(true, dest.toByteArray());
                    
                    WriteToConsole("LTV done");
                }
                catch (SignatureException ex)
                {
                    ex.printStackTrace();
                    throw ex;
                }
                
                UploadFile(true, links.get(dlLink), "", "", out);

                signed++;
                
                multiFrame.AdvanceTotalProgress();
                
                if(signed > 1) elapsed += System.currentTimeMillis() - start;
            }
       
        }
        catch(Throwable t)
        {    
            //HandleException
            throw t;
        }
    } 
    
    //</editor-fold>
    
    //<editor-fold desc="Desenvolvimento" defaultstate="collapsed">
    
    //<editor-fold desc="Single" defaultstate="collapsed">
    
    private static void DebugSignature()
    {
        try 
        {
            frame = new MainFrame(statusInitializing);
            frame.setTitle(applicationTitle);
            frame.setVisible(true);
            
            //url ="https://dev-web-ms.intra.utad.pt/Gesdoc/IGesDoc/Gesdoc/";
            //LogError("7845", "cpereira", "omg", new Exception("ISTO É UM TEXTO COM Ç E Ó E Õ"));
            
            FileInputStream fis = new FileInputStream("res/13-e-BI-2017.pdf");
            FileOutputStream fos = new FileOutputStream("res/13-e-BI-2017_ts_multicert.pdf");
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            
            SignPDF_Testes(fis, bos, true);
            
            byte[] res = bos.toByteArray();
            res = MakeLTV(false, res);
            
            fos.write(res);
        } 
        catch (FileNotFoundException ex) 
        {
            Logger.getLogger(SignUTADDesktop.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SignUTADDesktop.class.getName()).log(Level.SEVERE, null, ex);
        } catch (GeneralSecurityException ex) {
            Logger.getLogger(SignUTADDesktop.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(SignUTADDesktop.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PrivilegedActionException ex) {
            Logger.getLogger(SignUTADDesktop.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            System.exit(0);
        }
    }
    
    private static void SignPDF_Testes(InputStream src, OutputStream dest, boolean forceSignatureException)
    {
        try
        {
            byte[] out = null;
            byte[] srcBytes = IOUtils.toByteArray(src);
            try
            {
                out = SignPDF(srcBytes);
            }
            catch (SignatureException ex)
            {
                HandleException(ex, null, null);
                ex.printStackTrace();
            }
            catch (Throwable T)
            {
                HandleException(T, null, null);
            }
            
            dest.write(out);
        } 
        catch (IOException ex)
        {
            Logger.getLogger(SignUTADDesktop.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    //</editor-fold>
    
    //<editor-fold desc="Multi" defaultstate="collapsed">
    
    public static void DebugSignatureMulti()
    {
        try 
        {
            Map<String, String> data = new HashMap<String, String>();
            
            data.put("res/13-e-BI-2017.pdf", "res/multi/multi_1.pdf");
            data.put("res/13-e-BI-2017_1.pdf", "res/multi/multi_2.pdf");
            data.put("res/13-e-BI-2017_2.pdf", "res/multi/multi_3.pdf");
            
            Map<String, byte[]> res = SignPDF_Testes_Multi(data);
            
            for(String path : res.keySet())
            {
                FileOutputStream fos = new FileOutputStream(path);
                
                fos.write(res.get(path));
            }
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(SignUTADDesktop.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static Map<String, byte[]> SignPDF_Testes_Multi(Map<String, String> data)
    {
        try
        {
            frame = new MainFrame(statusInitializing);
            frame.setTitle(applicationTitle);
            frame.setVisible(true);
            
            Map<String, byte[]> res = new HashMap<String, byte[]>();
        
            PrivateKey pk = null;
            Certificate[] chain = null;

            KeyStore ks = null;

            String assinaturaCertifLabel = "";
            String provider = "";

            boolean pinOK = false;
            char[] pin = null;
            
            for(String s : data.keySet())
            {
                FileInputStream fis = new FileInputStream(s);
            
                byte[] out = null;
                byte[] srcBytes = IOUtils.toByteArray(fis);
                try
                {
                    ByteArrayInputStream src = new ByteArrayInputStream(srcBytes);
                    
                    // reader and stamper
                    PdfReader reader = new PdfReader(src);
                    WriteToConsole("PdfReader built");

                    AcroFields acro = reader.getAcroFields();

                    //ESTA LINHA FAZ COM QUE O signatureField DO JSON SEJA IGNORADO.
                    signatureField = GetSignatureFieldName(acro);

                    ByteArrayOutputStream dest = new ByteArrayOutputStream();
                    PdfStamper stamper = PdfStamper.createSignature(reader, dest, '\0', null, true);
                    WriteToConsole("PdfStamper built");

                    // appearance
                    PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
                    WriteToConsole("PdfSignatureAppearance built");

                    if(reason != null)
                    {
                        if(reasonCaption != null) appearance.setReasonCaption(reasonCaption);
                        else appearance.setReasonCaption("Motivo: ");
                        appearance.setReason(reason);

                        appearance.setLayer4Text(reasonCaption + reason);
                    }

                    frame.AdvanceProgress(25);

                    //<editor-fold desc="Tratar do signatureField e do visibleSignature" defaultstate="Collapsed">

                    if(signatureField != null && !signatureField.isEmpty())
                    {
                        appearance.setVisibleSignature(signatureField);

                        WriteToConsole("Signing on field " + signatureField);
                    }
                    else if(visibleSignature != null && visibleSignature) 
                    {
                        AcroFields acros = reader.getAcroFields();
                        String sigFieldName = "sig" + SignatureCount(acros);

                        appearance.setVisibleSignature(new Rectangle(llx, lly, urx, ury), 1, sigFieldName);

                        WriteToConsole("Signing with visible signature on urx: " + urx + "; ury: " + ury + "; llx: " + llx + "; lly: " + lly);
                    }


                    //</editor-fold>

                    frame.AdvanceProgress(25);

                    appearance.setSignatureCreator("Serviços de Informática e Comunicações - UTAD");

                    //Timestamp
                    // mvale - Instancia um cliente TSA customizado, que suporta o uso do servidor TSA da ANO caso o servidor TSA do Cartão de Cidadão falhe (quer por falta de timestamps, quer por outras razões)
                    // O código deste cliente TSA foi copiado do código-fonte do IText e modificado
                    // Esse código-fonte encontra-se disponível aqui: https://github.com/sunrenjie/itext/blob/master/itext/src/main/java/com/itextpdf/text/pdf/security/TSAClientBouncyCastle.java
                    TSAClient tsa = new TSAClientSignUTADDesktop("http://ts.cartaodecidadao.pt/tsa/server");
                    WriteToConsole("Built TSA client");

                    //OCSP
                    OcspClient ocsp = new OcspClientBouncyCastle();
                    WriteToConsole("Built OCSP client");
                    //CRL
                    //List<CrlClient> crlList = new ArrayList<CrlClient>();
                    //crlList.add(new CrlClientOnline(chain));

                    frame.AdvanceProgress(25);
                    frame.SetStatus(statusSigning);
                        
                    if(!pinOK)
                    {                        
                        if(Security.getProvider(POReIDConfig.POREID) == null) Security.addProvider(new POReIDProvider());
                        ks = KeyStore.getInstance(POReIDConfig.POREID);

                        assinaturaCertifLabel = POReIDConfig.ASSINATURA;
                        provider = POReIDConfig.POREID;
                        
                        do
                        {
                            CitizenCard cc = CardFactory.getCard();
                            CardSpecificReferences csr = cc.getCardSpecificReferences();
                            Pin p = csr.getCryptoReferences(PkAlias.ASSINATURA);

                            VerifyPinDialogController controller = VerifyPinDialogController.getInstance(CCConfig.TIMEOUT, p, csr.getLocale());
                            byte[] pinBytes = controller.askForPin();
                            pin = new String(pinBytes).toCharArray();
                            pinOK = cc.verifyPin(p, pinBytes);
                        } while(!pinOK);
                        
                        
                        ks.load(null, null);
                        WriteToConsole("Keystore loaded");
                        
                        //pk = (PrivateKey)ks.getKey(assinaturaCertifLabel, new char[] {'0', '0', '0', '0'});
                        pk = (PrivateKey)ks.getKey(assinaturaCertifLabel, pin);
                        //pk = (PrivateKey)ks.getKey(assinaturaCertifLabel, null);

                        chain = ks.getCertificateChain(assinaturaCertifLabel);
                        
                        if(chain == null)
                        {        
                            Security.removeProvider(POReIDConfig.POREID);
                            Security.addProvider(new POReIDProvider());

                            ks = KeyStore.getInstance(POReIDConfig.POREID);
                            ks.load(null,null);

                            pk = (PrivateKey)ks.getKey(POReIDConfig.ASSINATURA, null);
                            chain = ks.getCertificateChain(POReIDConfig.ASSINATURA);

                            if(chain == null)
                            {
                                throw new CertificateChainException();
                            }
                        }
                    }
                    
                    // digital signature
                    ExternalSignature es = new PrivateKeySignature(pk, "SHA-256", provider);
                    ExternalDigest digest = new ProviderDigest(null);

                    frame.AdvanceProgress(25);
                    WriteToConsole("Private key and chain acquired");

                    WriteToConsole("Signing");
                    frame.StartSignatureMessageTimer(statusTimerNoOCSP, 31);
                    //frame.StartDebugTimerSignature();

                    //MakeSignature.signDetached(appearance, digest, es, chain, null, ocsp, null, 0, MakeSignature.CryptoStandard.CMS);
                    //MakeSignature.signDetached(appearance, digest, es, chain, null, null, tsa, 0, MakeSignature.CryptoStandard.CMS);
                    MakeSignature.signDetached(appearance, digest, es, chain, null, null, null, 0, MakeSignature.CryptoStandard.CADES);

                    frame.StopSignatureMessageTimer();

                    src.close();

                    stamper.close();
                    reader.close();
                    WriteToConsole("Signed");
                    frame.SetStatus(statusSigned, true);
                    frame.SetProgressBarMax(100);
                    
                    out = dest.toByteArray();
                }
                catch (SignatureException ex)
                {
                    ex.printStackTrace();
                } catch (CardTerminalNotPresentException ex) {
                    Logger.getLogger(SignUTADDesktop.class.getName()).log(Level.SEVERE, null, ex);
                } catch (UnknownCardException ex) {
                    Logger.getLogger(SignUTADDesktop.class.getName()).log(Level.SEVERE, null, ex);
                } catch (CardNotPresentException ex) {
                    Logger.getLogger(SignUTADDesktop.class.getName()).log(Level.SEVERE, null, ex);
                } catch (CanceledSelectionException ex) {
                    Logger.getLogger(SignUTADDesktop.class.getName()).log(Level.SEVERE, null, ex);
                } catch (POReIDException ex) {
                    Logger.getLogger(SignUTADDesktop.class.getName()).log(Level.SEVERE, null, ex);
                } catch (PinEntryCancelledException ex) {
                    Logger.getLogger(SignUTADDesktop.class.getName()).log(Level.SEVERE, null, ex);
                } catch (PinTimeoutException ex) {
                    Logger.getLogger(SignUTADDesktop.class.getName()).log(Level.SEVERE, null, ex);
                } catch (PinBlockedException ex) {
                    Logger.getLogger(SignUTADDesktop.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                res.put(data.get(s), out);
            }
            
            return res;
        } 
        catch (IOException ex)
        {
            Logger.getLogger(SignUTADDesktop.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex)
        {
            Logger.getLogger(SignUTADDesktop.class.getName()).log(Level.SEVERE, null, ex);
        } catch (GeneralSecurityException ex)
        {
            Logger.getLogger(SignUTADDesktop.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CertificateChainException ex)
        {
            Logger.getLogger(SignUTADDesktop.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSignatureFieldsException ex)
        {
            Logger.getLogger(SignUTADDesktop.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    } 
    
    public static void TesteUploadsMulti(String[] args)
    {
        String message = "";
        
        try
        {            
            //DA PARA QUALQUER ENDEREÇO
            String argument = args[0].substring(args[0].indexOf("/", 13) + 1);
            byte[] argDecoded = Base64.getDecoder().decode(argument);
            String json = "";

            if (argDecoded == null) argDecoded = Base64.getDecoder().decode(argument.substring(0, argument.length() - 1));

            if (argDecoded != null) 
            {
                json = new String(argDecoded);
                ArgumentHandler arg = new ArgumentHandler(json);
                
                String multiSignJSON = getTextFromURL(arg.getMultipleUrlFileList());
                MultiSignatureArgumentHandler multiArg = new MultiSignatureArgumentHandler(multiSignJSON);

                HashMap<String, String> links = new HashMap<String, String>();
                for(String ref : multiArg.getFileRefs())
                {
                    String ulLink = multiArg.getBaseURLUpload() + "?token=" + arg.getMultipleToken() + "&ref=" + ref;
                    String dlLink = multiArg.getBaseURLDownload() + "?token=" + arg.getMultipleToken() + "&ref=" + ref;

                    byte[] file = DownloadFile(false, dlLink, "");
                    boolean uploaded = UploadFile(false, ulLink, "", "", file);
                }
            }
            else 
            {
                LogError("-1", "Aplicacao Assinatura UTAD", args[0], new Exception("Bad argument"));
                frame.ShowMessage(errorBadArguments, captionError);
                System.exit(0);
            }
        }
        catch(NullPointerException ex)
        {
            LogError("-1", "Assinatura Digital UTAD", args[0], ex);
            frame.ShowMessage(errorGeneralException + ex.getMessage(), captionError);
        }
        catch(Throwable T)
        {
            if (T.getClass() == OutOfMemoryError.class)
            {
                frame.SetAllowTimerMessageUpdates(false);
                frame.ShowMessage(errorGeneralException + T.getClass().toString(), captionError);
            }
            
            LogError("-1", "Assinatura Digital UTAD", args[0], T);
        }
        
        message = message + "";
        
        System.exit(0);
    }
    
    //</editor-fold>
    
    //</editor-fold>
    
    //<editor-fold desc="Pós-assinatura" defaultstate="collapsed">
    
    private static byte[] MakeLTV(boolean isMulti, byte[] src) throws IOException, GeneralSecurityException, DocumentException, PrivilegedActionException
    {
        OcspClientBouncyCastle ocsp = new OcspClientBouncyCastle();
        CrlClientOnline crl = new CrlClientOnline();
        
        //ORIGEM
        PdfReader readerLtv = new PdfReader(src);

        //DESTINO
        ByteArrayOutputStream dest = new ByteArrayOutputStream();

        PdfStamper stp = new PdfStamper(readerLtv, dest, '\0', true);

        if(isMulti)
        {
            multiFrame.SetFileStatus(statusLTVBegin, true);
            multiFrame.AdvanceFileProgress(37);
        }
        else
        {
            frame.SetStatus(statusLTVBegin, true);
            frame.AdvanceProgress(37);
        }
        
        LtvVerification ltvVer = stp.getLtvVerification();

        AcroFields fields = stp.getAcroFields();

        List<String> names = fields.getSignatureNames();
        String sigName = names.get(names.size() - 1);
        PdfPKCS7 pkcs7 = fields.verifySignature(sigName);
        
        if(isMulti)
        {
            multiFrame.AdvanceFileProgress(38);
        }
        else
        {
            frame.AdvanceProgress(38);
        }
        
        ltvVer.addVerification(sigName, ocsp, crl,
                            LtvVerification.CertificateOption.WHOLE_CHAIN,
                            LtvVerification.Level.OCSP_OPTIONAL_CRL,
                            LtvVerification.CertificateInclusion.YES);

        stp.close();
        readerLtv.close();
        
        if(isMulti)
        {
            multiFrame.SetFileStatus(statusLTVEnd);
            multiFrame.AdvanceFileProgress(25);
        }
        else
        {
            frame.SetStatus(statusLTVEnd);
            frame.AdvanceProgress(25);
        }
        
        //byte[] finalBytes = MakeLTVOnTimestamp(dest.toByteArray());
        
        return dest.toByteArray();
    }
    
    //</editor-fold>
    
    //<editor-fold desc="Campos de assinatura" defaultstate="collapsed">
    
    private static String GetSignatureFieldName(AcroFields acro) throws NoSignatureFieldsException
    {
        ArrayList<String> fields = acro.getBlankSignatureNames();
        
        if(fields.isEmpty()) 
        {
            //É melhor fazer assinatura invisível se não existirem campos.
            return null;
            
            
            //NÃO EXISTEM CAMPOS DE ASSINATURA DISPONÍVEIS
            //VERIFICA-SE SE EXISTE ALGUM CAMPO ASSINADO. SE NÃO EXISTIR, A ASSINATURA É TRATADA COMO INVISÍVEL.
            //ArrayList<String> anySignatureFields = acro.getSignatureNames();
            
            //if(anySignatureFields.isEmpty()) return null;
            //else throw new NoSignatureFieldsException();
        }
        
        fields.sort(String::compareToIgnoreCase);
        
        return fields.get(0);
    }
    
    private static int SignatureCount(AcroFields acro)
    {
        return acro.getSignatureNames().size() + acro.getBlankSignatureNames().size();
    }
    
    //</editor-fold>    
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Upload/Download">
    
    public static final String getTextFromURL(String url) throws MalformedURLException, IOException
    {
        URL website = new URL(url);
        URLConnection connection = website.openConnection();
        BufferedReader in = new BufferedReader(
                                new InputStreamReader(
                                    connection.getInputStream()));

        StringBuilder response = new StringBuilder();
        String inputLine;

        while ((inputLine = in.readLine()) != null) 
            response.append(inputLine);

        in.close();

        return response.toString();
    }
    
    public static final HttpURLConnection openDownloadConnection(URL url, String wsAuth) throws IOException{
        HttpURLConnection con = (HttpURLConnection) url.openConnection ();
        con.setDoInput(true);
        con.setDoOutput (true);
        
        con.setRequestMethod("GET");
        con.setRequestProperty ("Content-Type", "application/x-www-form-urlencoded");

        con.setRequestProperty("Accept", "application/pdf");
        
        if(wsAuth != null && wsAuth != "")
        {
            if(!wsAuth.contains(" ")) wsAuth = "Basic " + wsAuth;
            con.setRequestProperty("Authorization", wsAuth);
        }
        
        return con;
    }
    private static byte[] DownloadFile(boolean isMulti, String fullUrl, String wsAuth) throws MalformedURLException, IOException, RuntimeException, PrivilegedActionException
    {
        URLConnection c = null;
        try
        {
            fullUrl = Trim(fullUrl, "/");
            fullUrl = fullUrl.replace(" ", "%20");

            URL requestAddress = new URL(fullUrl);

            HttpURLConnection conn = openDownloadConnection(requestAddress, wsAuth);
            c = conn;

            int respcode = conn.getResponseCode();

            if (respcode > 299) 
            {
                throw new RuntimeException("Failed : HTTP error code : " + respcode);
            }

            InputStream in = conn.getInputStream(); 
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            //Saber o máximo da progress bar
            String value = conn.getHeaderField("UTAD-Content-Length");
            int valueInt = 0;
            if (value != null) {
                valueInt = Integer.parseInt(value);
            }
            else
            {
                value = conn.getHeaderField("Content-Length");
                if (value != null) {
                    valueInt = Integer.parseInt(value);
                }
            }

            if(isMulti)
            {
                multiFrame.SetFileStatus(statusDownloading, true);
                multiFrame.SetFileProgressBarMax(valueInt);
            }
            else
            {
                frame.SetStatus(statusDownloading, true);
                frame.SetProgressBarMax(valueInt);
            }

            byte[] b = new byte[1024];
            int count;

            while ((count = in.read(b)) > 0)
            {
                out.write(b, 0, count);
                if(isMulti)
                {
                    multiFrame.AdvanceFileProgress(count);
                }
                else
                {
                    frame.AdvanceProgress(count);
                }
            }
            if(isMulti)
            {
                multiFrame.SetFileStatus(statusDownloaded);
                multiFrame.SetFileProgressBarMax(100);
                multiFrame.SetFileProgress(100);
            }
            else
            {
                //Forçar a ficar a 100% no fim do download
                frame.SetProgressBarMax(100);
                frame.SetProgress(100);
            }
            out.close();
            conn.disconnect();
            
            return out.toByteArray();
        }
        catch(Throwable T)
        {
            if (c != null && c.getClass() == HttpURLConnection.class)
            {
                ((HttpURLConnection)c).disconnect();
            }
            throw T;
        }
    }
    private static byte[] DownloadFile(boolean isMulti, String url, String lib, String processNo, String filename, String token, String wsAuth) throws MalformedURLException, IOException, RuntimeException, PrivilegedActionException //DONE
    {      
        String fullUrl = (Trim(url, "/") + "/getFile(" + lib + "/" + processNo + "/" + filename + ")?token=" + token).replace(" ", "%20");
        
        return DownloadFile(isMulti, fullUrl, wsAuth);
    }
    
    public static final HttpURLConnection openUploadConnection(URL url, String wsAuth, int fileLength) throws IOException{
        HttpURLConnection con = (HttpURLConnection) url.openConnection ();
        con.setDoInput(true);
        con.setDoOutput (true);
                    
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Length", fileLength + "");
        con.setRequestProperty("Content-Type", "application/pdf");
        
        if(wsAuth != null && wsAuth != "")
        {
            if(!wsAuth.contains(" ")) wsAuth = "Basic " + wsAuth;
            con.setRequestProperty("Authorization", wsAuth);
        }
        return con;
    }
    private static boolean UploadFile(boolean isMulti, String fullUrl, String fileName, String wsAuth, byte[] file) throws MalformedURLException, IOException, PrivilegedActionException
    {
        try
        {
            if(isMulti)
            {
                multiFrame.SetFileStatus(statusUploading, true);
                multiFrame.SetFileProgressBarMax(100);
            }
            else
            {
                frame.SetStatus(statusUploading, true);
                frame.SetProgressBarMax(100);
            }
            if(fullUrl.contains("{FILENAME}"))
            {
                fullUrl = Trim(fullUrl, "/");
                fullUrl = fullUrl.replace(" ", "%20").replace("{FILENAME}", fileName);
            }
            
            URL requestAddress = new URL(fullUrl);

            HttpURLConnection conn = openUploadConnection(requestAddress, wsAuth, file.length);

            conn.setRequestProperty("Accept", "application/json;odata=verbose");

            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();

            os.write(file);

            int respcode = conn.getResponseCode();

            if (respcode > 299) 
            {
                throw new RuntimeException("Failed : HTTP error code : " + respcode);
            }
            if(isMulti)
            {
                multiFrame.SetFileProgress(100);
                multiFrame.SetFileStatus(statusUploaded);
            }
            else
            {
                frame.SetProgress(100);
                frame.SetStatus(statusUploaded);
            }
            
            conn.disconnect();

            return respcode <= 299;   
        
        }
        catch(Throwable T)
        {
            throw T;
        }
    }    
    //</editor-fold>

    //<editor-fold desc="GesDoc" defaultstate="collapsed">
    
    private static void Lacrar(String url, String id, String numInf, String fileName, String login, String token) throws MalformedURLException, IOException
    {
        frame.SetStatus(statusLacrar, true);
        frame.SetProgressBarMax(100);

        url = Trim(url, "/");
        URL requestAddress = new URL(url + "/lacrar?id=" + id + "&numinf=" + numInf + "&fileName=" + fileName + "&login=" + login + "&token=" + token);

        HttpURLConnection conn = (HttpURLConnection) requestAddress.openConnection();
        conn.setRequestMethod("GET");
        conn.connect();

        int respcode = conn.getResponseCode();
        if (respcode > 399)
        {
            frame.ShowMessage(errorGeneralException + "Erro " + respcode, captionError);
        }
        
        frame.SetProgress(100);
        frame.SetStatus(statusLacrado);
    }
    
    //</editor-fold>
    
    //<editor-fold desc="Versões" defaultstate="collapsed">
    
    private static String lastVersionFromWS = "";
    private static boolean IsLastVersion() throws MalformedURLException, ProtocolException, IOException
    {
        url = Trim(url, "/");

        URL requestAddress = new URL(url + "/latest");

        HttpURLConnection conn = (HttpURLConnection) requestAddress.openConnection();

        conn.setRequestMethod("GET");

        conn.connect();

        int respcode = conn.getResponseCode();

        if (respcode > 399)
        {
            frame.ShowMessage(errorGeneralException + "Erro " + respcode, captionError);
        }
        
        BufferedReader br =  new BufferedReader(new InputStreamReader(conn.getInputStream()));
        
        lastVersionFromWS = br.readLine().replace("\"", "");
        
        String[] thisVersionSplit = Version.split("\\.");
        String[] remoteVersionSplit = lastVersionFromWS.split("\\.");
        
        int vLength = thisVersionSplit.length;
        if(remoteVersionSplit.length < vLength) vLength = remoteVersionSplit.length;
        
        for(int i = 0; i < vLength; i++)
        {
            int thisVInt = Integer.parseInt(thisVersionSplit[i]);
            int remoteVInt = Integer.parseInt(remoteVersionSplit[i]);
            
            if(thisVInt > remoteVInt || (i + 1 >= vLength && thisVInt == remoteVInt)) return true;
            if(thisVInt < remoteVInt) return false;
            
        }
        
        return false;
    }
    
    private static void Update(String thisVersion, String requiredVersion, String link) throws IOException
    {        
        String thisPath = URLDecoder.decode(SignUTADDesktop.class.getProtectionDomain().getCodeSource().getLocation().getFile(), "UTF-8");
        thisPath = new File(thisPath).getParentFile().getPath() + File.separator;
        String updaterJarPath = thisPath + updaterJarName;
        
        updaterJarPath = updaterJarPath.replace('/', File.separatorChar);
        
        ProcessBuilder proc = new ProcessBuilder("java", "-jar", updaterJarName, thisVersion, requiredVersion, link);
        proc.directory(new File(thisPath));
        proc.start();
    }
    
    //</editor-fold>
    
    //<editor-fold desc="Error" defaultstate="collapsed">
    
    private static Boolean LogError(String idProcesso, String login, String link, Throwable T)
    {
        Boolean ret = false;
        
        try
        {
            String postUrl = url + "/logerro";
            URL obj = new URL(postUrl);
            if(postUrl.startsWith("https://"))
            {
                HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

                //add reuqest header
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type", "application/json");

                // Send post request
                ExceptionHandler handler = new ExceptionHandler(idProcesso, login, "Assinatura", link, T);

                String gson = handler.ToJson();
                
                gson = ToISO88591(gson);
                
                con.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.writeBytes(gson);
                wr.flush();
                wr.close();

                int responseCode = con.getResponseCode();
                ret = responseCode < 300;

                return ret;
            }
            else
            {
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                //add reuqest header
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type", "application/json");

                // Send post request
                ExceptionHandler handler = new ExceptionHandler(idProcesso, login, "Assinatura", link, T);

                String gson = handler.ToJson();

                con.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.writeBytes(gson);
                wr.flush();
                wr.close();

                int responseCode = con.getResponseCode();
                ret = responseCode < 300;


                return ret;
            }
        }
        catch(Throwable T1)
        {
            return false;
        }
    }
    
    private static void WriteToConsole(String s)
    {
        Console c = System.console();
        if (debug && c != null) c.writer().print(s);
    }
    
    private static void HandleException(Throwable t, String[] args)
    {
        HandleException(t, null, args);
    }
    
    private static void HandleException(Throwable t, ArgumentHandler arg, String[] args)
    {
        String id = "-1",
               login = "Assinatura Digital UTAD";
        
        if(arg != null)
        {
            login = arg.getLogin();
            id = arg.getID();
        }
        
        IFrameAssinatura fr = frame;
        if(fr == null) {
            fr = multiFrame;
        }
        if(fr == null) {
            fr = new MainFrame();
            ((MainFrame)fr).setTitle(applicationTitle);
        }
        
        try
        {
            throw t;
        }
        catch(SignatureException exc)
        {
            if(exc.getCause() != null)
            {
                try
                {
                    throw t.getCause();
                }
                catch(PinEntryCancelledException ex)
                {
                    fr.SetAllowTimerMessageUpdates(true);
                    fr.ShowMessage(errorCanceled, captionInfo);
                }
                catch(PinBlockedException ex)
                {
                    fr.SetAllowTimerMessageUpdates(true);
                    fr.ShowMessage(errorLockedPin, captionError);
                }
                catch (PinTimeoutException ex)
                {
                    fr.SetAllowTimerMessageUpdates(false);
                    fr.ShowMessage(errorSignTimeout, captionInfo);
                }
                catch(Throwable T)
                {
                    fr.SetAllowTimerMessageUpdates(false);
                    fr.ShowMessage(errorSignature, captionError);
                }
            }
            else
            {
                fr.SetAllowTimerMessageUpdates(false);
                fr.ShowMessage(errorGeneral, captionError);
            }
        }
        catch(OutOfMemoryError ex)
        {
            fr.SetAllowTimerMessageUpdates(false);
            fr.ShowMessage(errorGeneral, captionError);
        }
        catch(BadArgumentException ex)
        {
            fr.SetAllowTimerMessageUpdates(true);
            fr.ShowMessage(errorBadArguments, captionError);
        }
        catch(CertificateRevokedException ex)
        {
            fr.SetAllowTimerMessageUpdates(true);
            fr.ShowMessage(errorCertRevoked, captionError);
        }
        catch(InvalidOSException ex)
        {
            fr.SetAllowTimerMessageUpdates(true);
            fr.ShowMessage(errorInvalidOS, captionError);
        }
        catch(PinBlockedException ex) //Estas 3 estão aqui também porque podem ser lançadas dentro do MakeSignature, vindo numa SignatureException e sendo apanhadas acima, ou sozinhas quando o PIN é pedido manualmente na assinatura em série, sendo apanhadas aqui
        {
            fr.SetAllowTimerMessageUpdates(true);
            fr.ShowMessage(errorLockedPin, captionError);
        }
        catch (PinEntryCancelledException ex)
        {
            fr.SetAllowTimerMessageUpdates(true);
            fr.ShowMessage(errorCanceled, captionInfo);
        }
        catch (PinTimeoutException ex)
        {
            fr.SetAllowTimerMessageUpdates(false);
            fr.ShowMessage(errorSignTimeout, captionInfo);
        }
        catch (SecurityException ex)
        {
            fr.SetAllowTimerMessageUpdates(false);
            fr.ShowMessage(errorCheckCard, captionError);
        }
        catch(NotLastVersionException ex)
        {
            try
            {
                Update(ex.getThisVersion(), ex.getRequiredVersion(), args[0]);
            }
            catch(IOException io)
            {     
                t = new Exception("erro update", io);

                fr.SetAllowTimerMessageUpdates(false);
                fr.ShowMessage(errorNoUpdateJar, captionError);
            }
        }
        catch(NoSignatureFieldsException ex)
        {
            fr.SetAllowTimerMessageUpdates(false);
            fr.ShowMessage(errorNoSignatureFields, captionError);
        }
        catch (DocumentException
        | IOException
        | RuntimeException
        | GeneralSecurityException
        | PrivilegedActionException
        | CertificateChainException ex)
        {
            Throwable exCause = ex.getCause();

            if (exCause != null)
            {
                if (exCause.getClass() == java.lang.SecurityException.class)
                {
                    fr.SetAllowTimerMessageUpdates(false);
                    fr.ShowMessage(errorCheckCard, captionError);
                }
                else
                {
                    fr.SetAllowTimerMessageUpdates(false);
                    fr.ShowMessage(errorGeneralException + exCause.getClass().toString(), captionError);
                }
            }
            else
            {
                fr.SetAllowTimerMessageUpdates(false);
                fr.ShowMessage(errorGeneralException + ex.getClass().toString(), captionError);
            }
        }
        catch(Throwable T)
        {
            if (T.getClass() == OutOfMemoryError.class)
            {
                fr.SetAllowTimerMessageUpdates(false);
                fr.ShowMessage(errorGeneralException + T.getClass().toString(), captionError);
            }
        }
        
        LogError(id, login, args[0], t);
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Tools">
    
    private static String ToISO88591(String s) throws UnsupportedEncodingException
    {
        return new String(s.getBytes("UTF-8"), "ISO-8859-1");
    }
    
    private static String Trim(String text, String toReplace) //DONE
    {
        while(text.endsWith(toReplace))
        {
            text = text.replaceAll(toReplace + "$|^" + toReplace, "");
        }
        
        return text;
    }
    
    //</editor-fold>
}
