/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.utad.application.desktopsign.Tools;

import com.google.gson.*;


/**
 *
 * @author cpereira
 */

/*

{"SPLib":"coisa","Processo":"coisa","Filename":"coisa","WSUrl":"coisa","WSDomain":"intra","WSUser":"coisa","WSPassword":"mudarPass","WSToken":"token"}
{"SPLib":"coisa","Processo":"coisa","Filename":"coisa","WSUrl":"coisa","WSToken":"token"}
{"SPLib":"coisa","Processo":"coisa","Filename":"coisa","WSUrl":"coisa","WSToken":"token","urx":"0","ury":"0","llx":"0","llx":"0","SignatureField":"sig0","ReasonCaption":"Despacho: ","Reason":"Favorável"}
*/

public class ArgumentHandler {

    private String SPLib;
    public String getSPLib() {
        return SPLib;
    }
    
    private String Processo;
    public String getProcesso() {
        return Processo;
    }

    private String Filename;
    public String getFilename() {
        return Filename;
    }

    private String WSUrl;
    public String getWSUrl() {
        return WSUrl;
    }

    private String DownloadUrl;
    public String getDownloadUrl() {
        return DownloadUrl;
    }

    private String UploadUrl;
    public String getUploadUrl() {
        return UploadUrl;
    }
/*
    private String WSDomain;
    public String getWSDomain() {
        return WSDomain;
    }

    private String WSUser;
    public String getWSUser() {
        return WSUser;
    }

    private String WSPassword;
    public String getWSPassword() {
        return WSPassword;
    }
*/
    private String WSToken;
    public String getWSToken() {
        return WSToken;
    }
    
    private String SignatureField;
    public String getSignatureField() {
        return SignatureField;
    }
    
    //<editor-fold desc="coordenadas">
    private Float urX;
    public Float getUrX() {
        return urX;
    }
    
    private Float urY;
    public Float getUrY() {
        return urY;
    }

    private Float llX;
    public Float getLlX() {
        return llX;
    }

    private Float llY;
    public Float getLlY() {
        return llY;
    }
    //</editor-fold>
    
    private String ReasonCaption;
    public String getReasonCaption() {
        return ReasonCaption;
    }
    
    private String Reason;
    public String getReason() {
        return Reason;
    }
    
    private String ID;
    public String getID() {
        return ID;
    }
    
    private String Login;
    public String getLogin() {
        return Login;
    }
    
    private String WSAuth;
    public String getWSAuth()
    {
        return WSAuth;
    }
    
    //<editor-fold defaultstate="collapsed" desc="Multi">
    
    private String urlfilelist;
    public String getMultipleUrlFileList()
    {
        return urlfilelist;
    }
    
    private boolean multiple;
    public boolean getMultiple()
    {
        return multiple;
    }
    
    private String token;
    public String getMultipleToken()
    {
        return token;
    }
    
    //</editor-fold>

    public ArgumentHandler(String rjson)
    {
        Gson g = new Gson();
        
        ArgumentHandler novo = g.fromJson(rjson, ArgumentHandler.class);
        
        SPLib = novo.SPLib;
        Processo = novo.Processo;
        Filename = novo.Filename;
        WSUrl = novo.WSUrl;
        
        DownloadUrl = novo.DownloadUrl;
        UploadUrl = novo.UploadUrl;
        /*
        WSDomain = novo.WSDomain;
        WSUser = novo.WSUser;
        WSPassword = novo.WSPassword;
        */
        WSToken = novo.WSToken;
        SignatureField = novo.SignatureField;
        
        urX = novo.urX;
        urY = novo.urY;
        llX = novo.llX;
        llY = novo.llY;
        
        ReasonCaption = novo.ReasonCaption;
        Reason = novo.Reason;
        
        ID = novo.ID;
        Login = novo.Login;
        
        WSAuth = novo.WSAuth;
        
        if((WSUrl != null && WSUrl != "") && (DownloadUrl == null || DownloadUrl == ""))
        {
            //PARA O IGesDoc
            DownloadUrl = (Trim(WSUrl, "/") + "/getFile(" + SPLib + "/" + Processo + "/" + Filename + ")?token=" + WSToken).replace(" ", "%20");
        }
        if((WSUrl != null && WSUrl != "") && (UploadUrl == null || UploadUrl == ""))
        {
            //PARA O IGesDoc
            UploadUrl = (Trim(WSUrl, "/") + "/AddProcessoAuthLacrar(" + Processo + "/{FILENAME})?categoria=Assinatura_GesDoc&assinado=true&token=" + WSToken + "&id=" + ID + "&username=" + Login).replace(" ", "%20").replace(" ", "%20");
        }
        
        multiple = novo.multiple;
        token = novo.token;
        urlfilelist = novo.urlfilelist;
    }
    
    private static String Trim(String text, String toReplace) //DONE
    {
        while(text.endsWith(toReplace))
        {
            text = text.replaceAll(toReplace + "$|^" + toReplace, "");
        }
        
        return text;
    }
}
