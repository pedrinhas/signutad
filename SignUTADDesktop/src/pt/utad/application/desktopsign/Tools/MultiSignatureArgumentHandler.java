/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.utad.application.desktopsign.Tools;

import com.google.gson.Gson;
import java.util.ArrayList;

/**
 *
 * @author claudio
 */
public class MultiSignatureArgumentHandler {
    
    private String urldownload;
    public String getBaseURLDownload()
    {
        return urldownload;
    }
    
    private String urlupload;
    public String getBaseURLUpload()
    {
        return urlupload;
    }
    
    private ArrayList<String> refs;
    public ArrayList<String> getFileRefs()
    {
        return refs;
    }
    
    public MultiSignatureArgumentHandler(String rjson)
    {
        Gson g = new Gson();
        
        MultiSignatureArgumentHandler novo = g.fromJson(rjson, MultiSignatureArgumentHandler.class);
        
        urldownload = novo.urldownload;
        urlupload = novo.urlupload;
        
        refs = novo.refs;
    }
    
    private static String Trim(String text, String toReplace) //DONE
    {
        while(text.endsWith(toReplace))
        {
            text = text.replaceAll(toReplace + "$|^" + toReplace, "");
        }
        
        return text;
    }
}
