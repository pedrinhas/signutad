/*
 * $Id$
 *
 * This file is part of the iText (R) project.
 * Copyright (c) 1998-2014 iText Group NV
 * Authors: Bruno Lowagie, Paulo Soares, et al.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation with the addition of the
 * following permission added to Section 15 as permitted in Section 7(a):
 * FOR ANY PART OF THE COVERED WORK IN WHICH THE COPYRIGHT IS OWNED BY
 * ITEXT GROUP. ITEXT GROUP DISCLAIMS THE WARRANTY OF NON INFRINGEMENT
 * OF THIRD PARTY RIGHTS
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA, 02110-1301 USA, or download the license from the following URL:
 * http://itextpdf.com/terms-of-use/
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License,
 * a covered work must retain the producer line in every PDF that is created
 * or manipulated using iText.
 *
 * You can be released from the requirements of the license by purchasing
 * a commercial license. Buying such a license is mandatory as soon as you
 * develop commercial activities involving the iText software without
 * disclosing the source code of your own applications.
 * These activities include: offering paid services to customers as an ASP,
 * serving PDFs on the fly in a web application, shipping iText with a closed
 * source product.
 *
 * For more information, please contact iText Software Corp. at this
 * address: sales@itextpdf.com
 */
package pt.utad.application.desktopsign.Tools;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLConnection;

import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.log.Logger;
import com.itextpdf.text.log.LoggerFactory;
import com.itextpdf.text.pdf.codec.Base64;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.cmp.PKIFailureInfo;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.tsp.TimeStampRequest;
import org.bouncycastle.tsp.TimeStampRequestGenerator;
import org.bouncycastle.tsp.TimeStampResponse;
import org.bouncycastle.tsp.TimeStampToken;
import org.bouncycastle.tsp.TimeStampTokenInfo;

/** mvale - Dependências adicionais necessárias */
import com.itextpdf.text.pdf.security.TSAClientBouncyCastle;
import com.itextpdf.text.pdf.security.TSAInfoBouncyCastle;
import com.itextpdf.text.pdf.security.BouncyCastleDigest;
import com.itextpdf.text.pdf.security.DigestAlgorithms;
import com.itextpdf.text.pdf.security.TSAClient;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.KeyManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.SSLContext;

import java.security.KeyStore;
import java.io.FileInputStream;
import java.security.SecureRandom;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.UnrecoverableKeyException;
import java.security.KeyManagementException;
import java.text.DateFormat;
import java.util.Locale;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * Time Stamp Authority Client interface implementation using Bouncy Castle
 * org.bouncycastle.tsp package.
 * <p>
 * Created by Aiken Sam, 2006-11-15, refactored by Martin Brunecky, 07/15/2007
 * for ease of subclassing.
 * </p>
 * @since	2.1.6
 */
public class TSAClientSignUTADDesktop implements TSAClient {

	/** The Logger instance. */
    private static final Logger LOGGER = LoggerFactory.getLogger(TSAClientBouncyCastle.class);
    
    /** URL of the Time Stamp Authority */
	protected String tsaURL;
	
	/** TSA Username */
    protected String tsaUsername;
    
    /** TSA password */
    protected String tsaPassword;

    /** An interface that allows you to inspect the timestamp info. */
    protected TSAInfoBouncyCastle tsaInfo;
    
    /** The default value for the hash algorithm */
    public static final int DEFAULTTOKENSIZE = 4096;
    
    /** Estimate of the received time stamp token */
    protected int tokenSizeEstimate;
    
    /** The default value for the hash algorithm */
    public static final String DEFAULTHASHALGORITHM = "SHA-256";
    
    /** Hash algorithm */
    protected String digestAlgorithm;
    
    /**
     * Creates an instance of a TSAClient that will use BouncyCastle.
     * @param url String - Time Stamp Authority URL (i.e. "http://tsatest1.digistamp.com/TSA")
     */
    public TSAClientSignUTADDesktop(String url) {
        this(url, null, null, DEFAULTTOKENSIZE, DEFAULTHASHALGORITHM);
    }
    
    /**
     * Creates an instance of a TSAClient that will use BouncyCastle.
     * @param url String - Time Stamp Authority URL (i.e. "http://tsatest1.digistamp.com/TSA")
     * @param username String - user(account) name
     * @param password String - password
     */
    public TSAClientSignUTADDesktop(String url, String username, String password) {
        this(url, username, password, 4096, DEFAULTHASHALGORITHM);
    }
    
    /**
     * Constructor.
     * Note the token size estimate is updated by each call, as the token
     * size is not likely to change (as long as we call the same TSA using
     * the same imprint length).
     * @param url String - Time Stamp Authority URL (i.e. "http://tsatest1.digistamp.com/TSA")
     * @param username String - user(account) name
     * @param password String - password
     * @param tokSzEstimate int - estimated size of received time stamp token (DER encoded)
     */
    public TSAClientSignUTADDesktop(String url, String username, String password, int tokSzEstimate, String digestAlgorithm) {
        this.tsaURL       = url;
        this.tsaUsername  = username;
        this.tsaPassword  = password;
        this.tokenSizeEstimate = tokSzEstimate;
        this.digestAlgorithm = digestAlgorithm;
    }
    
	/**
	 * @param tsaInfo the tsaInfo to set
	 */
	public void setTSAInfo(TSAInfoBouncyCastle tsaInfo) {
		this.tsaInfo = tsaInfo;
	}

	/**
     * Get the token size estimate.
     * Returned value reflects the result of the last succesfull call, padded
     * @return an estimate of the token size
     */
    public int getTokenSizeEstimate() {
        return tokenSizeEstimate;
    }

    /**
     * Gets the MessageDigest to digest the data imprint
     * @return the digest algorithm name
     */
    public MessageDigest getMessageDigest() throws GeneralSecurityException {
        return new BouncyCastleDigest().getMessageDigest(digestAlgorithm);
    }
    
    /**
     * Get RFC 3161 timeStampToken.
     * Method may return null indicating that timestamp should be skipped.
     * @param imprint data imprint to be time-stamped
     * @return encoded, TSA signed data of the timeStampToken
     * @throws IOException
     * @throws TSPException 
     */
    public byte[] getTimeStampToken(byte[] imprint) throws IOException, TSPException, KeyStoreException, NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException, KeyManagementException {
        try {  
            byte[] respBytes = null;
                // Setup the time stamp request
                TimeStampRequestGenerator tsqGenerator = new TimeStampRequestGenerator();
                tsqGenerator.setCertReq(true);
                // tsqGenerator.setReqPolicy("1.3.6.1.4.1.601.10.3.1");
                BigInteger nonce = BigInteger.valueOf(System.currentTimeMillis());
                TimeStampRequest request = tsqGenerator.generate(new ASN1ObjectIdentifier(DigestAlgorithms.getAllowedDigests(digestAlgorithm)), imprint, nonce);
                byte[] requestBytes = request.getEncoded();

                // Call the communications layer
                respBytes = getTSAResponse(requestBytes, tsaURL);

                // mvale - Se a resposta do servidor TSA for igual a null, significa que ocorreu algum erro no servidor
                // Ou o servidor TSA pode ter ficado sem timestamps, ou pode ter ocorrido algum erro na ligação com o servidor TSA
                // Portanto tenta obter, com o mesmo pedido de timestamp, um timestamp do servidor TSA da ANO
                if(respBytes == null) {
                    // mvale - Verifica primeiro se o servidor TSA da ANO se encontra na validade e se possui timestamps para fornecer
                        if(checkTSAExpirationAndBalance("https://tsa.multicert.com/pec/requests/00000PT501345361E5DYIEIE") == true) {
                        respBytes = getTSAResponse(requestBytes, "https://tsa.multicert.com/pec/pecsvcq/00000PT501345361E5DYIEIE"); 
                    }
                }

                // Handle the TSA response
                TimeStampResponse response = new TimeStampResponse(respBytes);

                // validate communication level attributes (RFC 3161 PKIStatus)
                response.validate(request);
                PKIFailureInfo failure = response.getFailInfo();
                int value = (failure == null) ? 0 : failure.intValue();
                if (value != 0) {
                    // @todo: Translate value of 15 error codes defined by PKIFailureInfo to string
                    throw new IOException(MessageLocalization.getComposedMessage("invalid.tsa.1.response.code.2", tsaURL, String.valueOf(value)));
                }
                // @todo: validate the time stap certificate chain (if we want
                //        assure we do not sign using an invalid timestamp).

                // extract just the time stamp token (removes communication status info)
                TimeStampToken  tsToken = response.getTimeStampToken();
                if (tsToken == null) {
                    throw new IOException(MessageLocalization.getComposedMessage("tsa.1.failed.to.return.time.stamp.token.2", tsaURL, response.getStatusString()));
                }
                TimeStampTokenInfo tsTokenInfo = tsToken.getTimeStampInfo(); // to view details
                byte[] encoded = tsToken.getEncoded();

                LOGGER.info("Timestamp generated: " + tsTokenInfo.getGenTime());
                if (tsaInfo != null) {
                    tsaInfo.inspectTimeStampTokenInfo(tsTokenInfo);
                }
                // Update our token size estimate for the next call (padded to be safe)
                this.tokenSizeEstimate = encoded.length + 32;
                return encoded;
        }
        catch(Throwable t) {
            // mvale - Se ocorrer algum erro inesperado, simplesmente retorna null
            // Isto permite que o programa não pare - nestes casos, o que acontece é que simplesmente o documento é assinado sem timestamp
            return null;
        }
    }
    
    /**
     * Get timestamp token - communications layer
     * @return - byte[] - TSA response, raw bytes (RFC 3161 encoded)
     * @throws IOException 
     */
    protected boolean checkTSAExpirationAndBalance(String tsaURL) {
        try {
            boolean isValid = false;
            
            String tsaANOExpirationDateString = "23-08-2029";
            DateFormat tsaANOExpirationDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
            Date tsaANOExpirationDate = tsaANOExpirationDateFormat.parse(tsaANOExpirationDateString);
            Date currentDate = new Date();
            
            if(currentDate.before(tsaANOExpirationDate) == true) {
                // Setup the TSA connection
                URL url = new URL(tsaURL);
                URLConnection tsaConnection = null;
                try {
                        // mvale - Carrega os certificados de autenticação da ANO para os usar com o pedido HTTPS ao servidor TSA da ANO
                        KeyStore clientStore = KeyStore.getInstance("JKS");
                        clientStore.load(TSAClientSignUTADDesktop.class.getClassLoader().getResourceAsStream("certificadoAnoExterno.jks"), "qmQLB3U2oW5e+r4K".toCharArray());
                        KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
                        kmf.init(clientStore, "qmQLB3U2oW5e+r4K".toCharArray());
                        KeyManager[] kms = kmf.getKeyManagers();

                        // mvale - Obtém o caminho de instalação do próprio Java e combina-o com o caminho da pasta da truststore do Java
                        KeyStore trustStore = KeyStore.getInstance("JKS");
                        String trustStorePath = System.getProperty("java.home") + "\\lib\\security\\cacerts";
                        trustStore.load(new FileInputStream(trustStorePath), "changeit".toCharArray());

                        TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                        tmf.init(trustStore);
                        TrustManager[] tms = tmf.getTrustManagers();

                        final SSLContext sslContext = SSLContext.getInstance("TLS");
                        sslContext.init(kms,tms,new SecureRandom());
                        SSLContext.setDefault(sslContext);

                        HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
                        tsaConnection = (HttpsURLConnection) url.openConnection();
                }
                catch (IOException ioe) {
                    throw new IOException(MessageLocalization.getComposedMessage("failed.to.get.tsa.response.from.1", tsaURL));
                }
                tsaConnection.setDoInput(true);
                tsaConnection.setDoOutput(false);
                tsaConnection.setUseCaches(false);
                tsaConnection.setRequestProperty("Content-Type", "application/json");
                
                // mvale - Obtém a resposta JSON do servidor TSA e converte-a para uma string
                InputStream inp = tsaConnection.getInputStream();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                int bytesRead = 0;
                while ((bytesRead = inp.read(buffer, 0, buffer.length)) >= 0) {
                    baos.write(buffer, 0, bytesRead);
                }
                String respString = baos.toString();
                
                // mvale - Para não estar com todo o trabalho em adicionar uma biblioteca de JSON ao programa, simplesmente faz um split à string de resposta e obtém o número de timestamps
                String tsaBalanceString = respString.substring(respString.indexOf("\"timestamps\":") + "\"timestamps\":".length(), respString.indexOf(",\"creationDate\""));
                if(Integer.parseInt(tsaBalanceString) > 0) {
                    isValid = true;
                }
            }
            
            return isValid;
        }
        catch(Throwable t) {
            return false;
        }
    }
    
    /**
     * Get timestamp token - communications layer
     * @return - byte[] - TSA response, raw bytes (RFC 3161 encoded)
     * @throws IOException 
     */
    protected byte[] getTSAResponse(byte[] requestBytes, String tsaURL) throws IOException, KeyStoreException, NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException, KeyManagementException {
        try {
            // Setup the TSA connection
            URL url = new URL(tsaURL);
            URLConnection tsaConnection = null;
            // mvale - Dependendo do protocolo do URL do servidor TSA passado como parâmetro, cria conexões de maneira diferente
            // Por exemplo, se o servidor TSA for HTTPS, é preciso carregar certificados de autenticação
            try {
                    if ("https".equals(url.getProtocol())) { 
                        // mvale - Carrega os certificados de autenticação da ANO para os usar com o pedido HTTPS ao servidor TSA da ANO
                        KeyStore clientStore = KeyStore.getInstance("JKS");
                        clientStore.load(TSAClientSignUTADDesktop.class.getClassLoader().getResourceAsStream("certificadoAnoExterno.jks"), "qmQLB3U2oW5e+r4K".toCharArray());
                        KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
                        kmf.init(clientStore, "qmQLB3U2oW5e+r4K".toCharArray());
                        KeyManager[] kms = kmf.getKeyManagers();

                        // mvale - Obtém o caminho de instalação do próprio Java e combina-o com o caminho da pasta da truststore do Java
                        KeyStore trustStore = KeyStore.getInstance("JKS");
                        String trustStorePath = System.getProperty("java.home") + "\\lib\\security\\cacerts";
                        trustStore.load(new FileInputStream(trustStorePath), "changeit".toCharArray());

                        TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                        tmf.init(trustStore);
                        TrustManager[] tms = tmf.getTrustManagers();

                        final SSLContext sslContext = SSLContext.getInstance("TLS");
                        sslContext.init(kms,tms,new SecureRandom());
                        SSLContext.setDefault(sslContext);

                        HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
                        tsaConnection = (HttpsURLConnection) url.openConnection();
                    } else if ("http".equals(url.getProtocol())) {
                        tsaConnection = (URLConnection) url.openConnection();
                    }
            }
            catch (IOException ioe) {
                throw new IOException(MessageLocalization.getComposedMessage("failed.to.get.tsa.response.from.1", tsaURL));
            }
            tsaConnection.setDoInput(true);
            tsaConnection.setDoOutput(true);
            tsaConnection.setUseCaches(false);
            tsaConnection.setRequestProperty("Content-Type", "application/timestamp-query");
            //tsaConnection.setRequestProperty("Content-Transfer-Encoding", "base64");
            tsaConnection.setRequestProperty("Content-Transfer-Encoding", "binary");

            if ((tsaUsername != null) && !tsaUsername.equals("") ) {
                String userPassword = tsaUsername + ":" + tsaPassword;
                tsaConnection.setRequestProperty("Authorization", "Basic " +
                    Base64.encodeBytes(userPassword.getBytes(), Base64.DONT_BREAK_LINES));
            }
            OutputStream out = tsaConnection.getOutputStream();
            out.write(requestBytes);
            out.close();

            // Get TSA response as a byte array
            InputStream inp = tsaConnection.getInputStream();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int bytesRead = 0;
            while ((bytesRead = inp.read(buffer, 0, buffer.length)) >= 0) {
                baos.write(buffer, 0, bytesRead);
            }
            byte[] respBytes = baos.toByteArray();

            String encoding = tsaConnection.getContentEncoding();
            if (encoding != null && encoding.equalsIgnoreCase("base64")) {
                respBytes = Base64.decode(new String(respBytes));
            }
            return respBytes;
        }
        catch(Throwable t) {
            // mvale - Se ocorrer algum erro inesperado, simplesmente retorna null
            // Isto permite que o programa não pare - nestes casos, o que acontece é que o programa pode tentar de novo com o servidor TSA da ANO
            // Se nem com o servidor TSA da ANO o programa conseguir obter um timestamp válido, o que acontece é que simplesmente o documento é assinado sem timestamp
            return null;
        }
    }    
}