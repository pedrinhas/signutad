#!/bin/sh

keytool -import -trustcacerts -alias 'baltimore cybertrust root' -file 'baltimore cybertrust root.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'cartão de cidadão 001' -file 'cartão de cidadão 001.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'cartão de cidadão 002' -file 'cartão de cidadão 002.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'cartão de cidadão 003' -file 'cartão de cidadão 003.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'cartão de cidadão 004' -file 'cartão de cidadão 004.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de assinatura digital qualificada do cartão de cidadão 0001' -file 'ec de assinatura digital qualificada do cartão de cidadão 0001.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de assinatura digital qualificada do cartão de cidadão 0002' -file 'ec de assinatura digital qualificada do cartão de cidadão 0002.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de assinatura digital qualificada do cartão de cidadão 0003' -file 'ec de assinatura digital qualificada do cartão de cidadão 0003.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de assinatura digital qualificada do cartão de cidadão 0004' -file 'ec de assinatura digital qualificada do cartão de cidadão 0004.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de assinatura digital qualificada do cartão de cidadão 0005' -file 'ec de assinatura digital qualificada do cartão de cidadão 0005.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de assinatura digital qualificada do cartão de cidadão 0006' -file 'ec de assinatura digital qualificada do cartão de cidadão 0006.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de assinatura digital qualificada do cartão de cidadão 0007' -file 'ec de assinatura digital qualificada do cartão de cidadão 0007.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de assinatura digital qualificada do cartão de cidadão 0008' -file 'ec de assinatura digital qualificada do cartão de cidadão 0008.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de assinatura digital qualificada do cartão de cidadão 0009' -file 'ec de assinatura digital qualificada do cartão de cidadão 0009.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de assinatura digital qualificada do cartão de cidadão 0010' -file 'ec de assinatura digital qualificada do cartão de cidadão 0010.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de assinatura digital qualificada do cartão de cidadão 0011' -file 'ec de assinatura digital qualificada do cartão de cidadão 0011.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de assinatura digital qualificada do cartão de cidadão 0012' -file 'ec de assinatura digital qualificada do cartão de cidadão 0012.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de assinatura digital qualificada do cartão de cidadão 0013' -file 'ec de assinatura digital qualificada do cartão de cidadão 0013.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de assinatura digital qualificada do cartão de cidadão 0014' -file 'ec de assinatura digital qualificada do cartão de cidadão 0014.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de assinatura digital qualificada do cartão de cidadão 0015' -file 'ec de assinatura digital qualificada do cartão de cidadão 0015.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de autenticação do cartão de cidadão 0001' -file 'ec de autenticação do cartão de cidadão 0001.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de autenticação do cartão de cidadão 0002' -file 'ec de autenticação do cartão de cidadão 0002.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de autenticação do cartão de cidadão 0003' -file 'ec de autenticação do cartão de cidadão 0003.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de autenticação do cartão de cidadão 0004' -file 'ec de autenticação do cartão de cidadão 0004.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de autenticação do cartão de cidadão 0005' -file 'ec de autenticação do cartão de cidadão 0005.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de autenticação do cartão de cidadão 0006' -file 'ec de autenticação do cartão de cidadão 0006.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de autenticação do cartão de cidadão 0007' -file 'ec de autenticação do cartão de cidadão 0007.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de autenticação do cartão de cidadão 0008' -file 'ec de autenticação do cartão de cidadão 0008.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de autenticação do cartão de cidadão 0009' -file 'ec de autenticação do cartão de cidadão 0009.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de autenticação do cartão de cidadão 0010' -file 'ec de autenticação do cartão de cidadão 0010.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de autenticação do cartão de cidadão 0011' -file 'ec de autenticação do cartão de cidadão 0011.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de autenticação do cartão de cidadão 0012' -file 'ec de autenticação do cartão de cidadão 0012.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de autenticação do cartão de cidadão 0013' -file 'ec de autenticação do cartão de cidadão 0013.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de autenticação do cartão de cidadão 0014' -file 'ec de autenticação do cartão de cidadão 0014.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ec de autenticação do cartão de cidadão 0015' -file 'ec de autenticação do cartão de cidadão 0015.cert' -keystore t.jks
keytool -import -trustcacerts -alias 'ecraizestado' -file 'ecraizestado.cert' -keystore t.jks