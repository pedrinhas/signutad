#!/bin/sh


#jarsigner -storetype PKCS12 -storepass \$p4s7i10# -keystore /media/claudio/Disco/Mega/work/SIC/CERT/SIC-UTAD-KS.p12 /media/claudio/Disco/WORK/signutad/SignUTADDesktop/store/Assinatura\ Digital\ UTAD.jar gesdoc.utad.pt;
echo
echo "signing store/Assinatura Digital UTAD.jar"
echo
jarsigner -storetype PKCS12 -storepass \$c4s4t4s8# -keystore ../cert/utad_codesign.p12 -tsa http://timestamp.digicert.com store/Assinatura\ Digital\ UTAD.jar 1188203a77ce42098b8d2c25400ccfeb

echo
echo "signing ../SignUTADUpdater/dist/SignUTADUpdater.jar"
echo
jarsigner -storetype PKCS12 -storepass \$c4s4t4s8# -keystore ../cert/utad_codesign.p12 -tsa http://timestamp.digicert.com ../SignUTADUpdater/dist/SignUTADUpdater.jar 1188203a77ce42098b8d2c25400ccfeb