#!/bin/sh

cd $(dirname $BASH_SOURCE)

rm -rf Assinatura\ Digital\ UTAD.app
cp -a ADUTAD_plist.app Assinatura\ Digital\ UTAD.app

cp -a ../Assinatura\ Digital\ UTAD.jar Assinatura\ Digital\ UTAD.app/Contents/Resources/
cp -a ../updater.jar Assinatura\ Digital\ UTAD.app/Contents/Resources/
cp -a ../run.sh Assinatura\ Digital\ UTAD.app/Contents/Resources/

open -a "Script Editor" Assinatura\ Digital\ UTAD.app/

#codesign -s "Universidade de Trás-os-Montes e Alto Douro" Assinatura\ Digital\ UTAD.app

#codesign -dv --verbose=4 Assinatura\ Digital\ UTAD.app