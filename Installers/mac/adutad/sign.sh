#!/bin/sh

cd $(dirname $BASH_SOURCE)

codesign -s -f "Mac Developer: Cláudio Pereira (VX4998PPJF)" Assinatura\ Digital\ UTAD.app

codesign -vv --deep-verify Assinatura\ Digital\ UTAD.app/
codesign -dvv Assinatura\ Digital\ UTAD.app/