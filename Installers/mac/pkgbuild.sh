#!/bin/sh

unzip -o adutad.zip

cd adutad

rm -rf Assinatura\ Digital\ UTAD.app
cp -a adutad.app Assinatura\ Digital\ UTAD.app

#cp -a ../Assinatura\ Digital\ UTAD.jar Assinatura\ Digital\ UTAD.app/Contents/Resources/
#cp -a ../updater.jar Assinatura\ Digital\ UTAD.app/Contents/Resources/
#cp -a ../run.sh Assinatura\ Digital\ UTAD.app/Contents/Resources/

codesign --verbose --force --deep --sign "Developer ID Application: Universidade de Trás-os-Montes e Alto Douro (5JB98T2657)" Assinatura\ Digital\ UTAD.app/

../fileicon set Assinatura\ Digital\ UTAD.app ../logosign.png

cd ..

#fazer codesign

pkgbuild --scripts scripts/ --identifier pt.utad.utadsign --install-location /Applications --component adutad/Assinatura\ Digital\ UTAD.app adutad_unsigned.pkg

productsign --sign "Developer ID Installer: Universidade de Trás-os-Montes e Alto Douro (5JB98T2657)" adutad_unsigned.pkg Assinatura\ Digital\ UTAD.pkg

#./fileicon set Assinatura\ Digital\ UTAD.pkg logosign.png

rm -rf adutad/Assinatura\ Digital\ UTAD.app
rm adutad_unsigned.pkg
#rm Assinatura\ Digital\ UTAD.jar
#rm updater.jar

rm adutad.zip

zip -r -X adutad.zip adutad

rm -rf adutad
