#!/bin/bash

if [[ $# -gt 0 || -z $1 ]]; then
	cd $1
fi

echo "cp -ar ../SignUTADDesktop/store/Assinatura\ Digital\ UTAD.jar Assinatura\ Digital\ UTAD.jar"
cp -ar ../SignUTADDesktop/store/Assinatura\ Digital\ UTAD.jar Assinatura\ Digital\ UTAD.jar
echo "cp -ar ../SignUTADUpdater/dist/SignUTADUpdater.jar updater.jar"
cp -ar ../SignUTADUpdater/dist/SignUTADUpdater.jar updater.jar

cd linux
echo "rm Assinatura\ Digital\ UTAD\ v*.zip"
rm -f Assinatura\ Digital\ UTAD*.zip
echo "zip Assinatura Digital UTAD.zip Assinatura Digital UTAD.jar install.sh uninstall.sh"
zip Assinatura\ Digital\ UTAD.zip install.sh uninstall.sh

rm -f updater.jar
rm -f Assinatura\ Digital\ UTAD.jar
cd ..

echo
echo "OK!"
