#!/bin/sh

echo "A apagar a pasta do programa."
echo "rm -rf ~/.utadsign/"
rm -rf ~/.utadsign/

echo ""
echo "A remover associação do protocolo com o programa"
echo "rm ~/.local/share/applications/utadsign.desktop"
rm ~/.local/share/applications/utadsign.desktop

if [ $(cat ~/.local/share/applications/mimeapps.list | grep utadsign | wc -l) -ne 0 ]; then
	echo "cat ~/.local/share/applications/mimeapps.list | grep utadsign --invert-match > mimeapps.list.bck"
	cat ~/.local/share/applications/mimeapps.list | grep utadsign --invert-match > ~/.local/share/applications/mimeapps.list.bck
	echo "cp ~/.local/share/applications/mimeapps.list.bck ~/.local/share/applications/mimeapps.list"
	cp ~/.local/share/applications/mimeapps.list.bck ~/.local/share/applications/mimeapps.list
fi

echo "rm $HOME/Desktop/sign.sh"
rm $HOME/Desktop/sign.sh

echo ""
echo "A atualizar as aplicações por defeito."
echo "update-desktop-database ~/.local/share/applications"
update-desktop-database ~/.local/share/applications