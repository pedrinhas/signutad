#!/bin/sh

echo "A criar a pasta para o programa."
echo "sudo mkdir -p ~/.utadsign"
mkdir -p ~/.utadsign

echo ""
echo "A copiar o programa."
echo "wget https://gesdoc.utad.pt/Installers/Assinatura/Assinatura\ Digital\ UTAD.jar -O ~/.utadsign/Assinatura\ Digital\ UTAD.jar"
wget https://gesdoc.utad.pt/Installers/Assinatura/Assinatura\ Digital\ UTAD.jar -O ~/.utadsign/Assinatura\ Digital\ UTAD.jar
echo "wget https://gesdoc.utad.pt/Installers/Assinatura/updater.jar -O ~/.utadsign/updater.jar"
wget https://gesdoc.utad.pt/Installers/Assinatura/updater.jar -O ~/.utadsign/updater.jar

echo "cp sign.sh $HOME/Desktop/"
cp sign.sh $HOME/Desktop/

echo ""
echo "A associar o protocolo com o programa"
echo "echo \[Desktop Entry]\nName=utadsign\nExec=sh $HOME/Desktop/sign.sh %u\nType=Application\nTerminal=false\nMimeType=x-scheme-handler/utadsign;\" > ~/.local/share/applications/utadsign.desktop"
echo "[Desktop Entry]\nName=utadsign\nExec=sh $HOME/Desktop/sign.sh %u\nType=Application\nTerminal=false\nMimeType=x-scheme-handler/utadsign;" > ~/.local/share/applications/utadsign.desktop

if [ $(cat ~/.local/share/applications/mimeapps.list | grep utadsign | wc -l) -eq 0 ]; then
	echo "awk '/Default Applications/ { print; print "x-scheme-handler/utadsign=utadsign.desktop"; next }1' ~/.local/share/applications/mimeapps.list > ~/.local/share/applications/mimeapps.list.bck"
	awk '/Default Applications/ { print; print "x-scheme-handler/utadsign=utadsign.desktop"; next }1' ~/.local/share/applications/mimeapps.list > ~/.local/share/applications/mimeapps.list.bck
	echo "cp ~/.local/share/applications/mimeapps.list.bck ~/.local/share/applications/mimeapps.list"
	cp ~/.local/share/applications/mimeapps.list.bck ~/.local/share/applications/mimeapps.list
fi

echo ""
echo "A atualizar as aplicações por defeito."
echo "update-desktop-database ~/.local/share/applications"
update-desktop-database ~/.local/share/applications