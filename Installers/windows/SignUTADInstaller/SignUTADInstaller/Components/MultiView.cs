﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SignUTADInstaller.Components
{
    public class MultiView : View
    {
        public event MultiViewChangedDelegate OnViewChanged;
        public event MultiViewChangedDelegate OnViewChanging;

        private List<View> views = new List<View>();

        public List<View> Views
        {
            get { return views; }
            private set { views = value; }
        }

        public int ActiveViewIndex
        {
            get
            {
                if (Views.Count == 0) return -1;
                return Views.IndexOf(Views.Where(x => x.Visible).First());
            }
        }

        public View ActiveView { get { return Views[ActiveViewIndex]; } }

        public bool IsLastView { get { return ActiveViewIndex == Views.Count - 1; } }

        public void AddView(View p)
        {
            p.Size = this.Size;
            p.Location = new System.Drawing.Point(0, 0);
            p.Parent = this;

            Views.Add(p);
            if (Views.Count == 1)
            {
                Views.First().Visible = true;
                Views.First().CanGoBack = false;
            }
        }
        public void RemoveView(int index)
        {
            Views.RemoveAt(index);
        }

        private void SetVisibleView(int index)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new System.Action(delegate()
                {
                    Views.ForEach(x => x.Visible = false);
                    Views[index].Visible = true;
                }));
            }
            else
            {

                Views.ForEach(x => x.Visible = false);
                Views[index].Visible = true;

            }

        }
        public void Next()
        {
            SetView(ActiveViewIndex + 1);
#if !VDI
            if (ActiveView is ViewInstallPath) Next();
#endif
        }
        public void Prev()
        {
            SetView(ActiveViewIndex - 1);

#if !VDI
            if (ActiveView is ViewInstallPath) Prev();
#endif
        }
        public void SetView(int index)
        {
            if (!ActiveView.CanAdvance) return;
            int toShow = index;
            if (toShow < Views.Count && toShow >= 0 || Views.OfType<ViewFinal>().Count() == 0)
            {
                this.OnViewChanging(ActiveView);
                this.SetVisibleView(toShow);
                this.OnViewChanged(ActiveView);
            }
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // MultiView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Name = "MultiView";
            this.Size = new System.Drawing.Size(527, 141);
            this.ResumeLayout(false);

        }
    }
}
