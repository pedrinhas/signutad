﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SignUTADInstaller.Components
{
    public partial class View : UserControl
    {
        public View()
        {
            InitializeComponent();

            CanAdvance = true;
            CanGoBack = true;
        }

        public bool CanAdvance { get; set; }
        public bool CanGoBack { get; set; }
    }
}
