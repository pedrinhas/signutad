﻿namespace SignUTADInstaller.Components
{
    partial class ViewInstallPath
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.bBrowse = new System.Windows.Forms.Button();
            this.tbPath = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(16, 16);
            this.label2.Margin = new System.Windows.Forms.Padding(16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(495, 48);
            this.label2.TabIndex = 2;
            this.label2.Text = "Por favor escolha o local onde quer instalar a aplicação.\r\n\r\nPode também usar o l" +
    "ocal pré-seleccionado.";
            // 
            // bBrowse
            // 
            this.bBrowse.Location = new System.Drawing.Point(436, 94);
            this.bBrowse.Margin = new System.Windows.Forms.Padding(3, 16, 16, 3);
            this.bBrowse.Name = "bBrowse";
            this.bBrowse.Size = new System.Drawing.Size(75, 23);
            this.bBrowse.TabIndex = 1;
            this.bBrowse.Text = "Browse";
            this.bBrowse.UseVisualStyleBackColor = true;
            this.bBrowse.Click += new System.EventHandler(this.bBrowse_Click);
            // 
            // tbPath
            // 
            this.tbPath.Location = new System.Drawing.Point(16, 96);
            this.tbPath.Margin = new System.Windows.Forms.Padding(16, 16, 3, 3);
            this.tbPath.Name = "tbPath";
            this.tbPath.Size = new System.Drawing.Size(391, 20);
            this.tbPath.TabIndex = 0;
            this.tbPath.Text = "PLACEHOLDER TEXT";
            // 
            // ViewInstallPath
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bBrowse);
            this.Controls.Add(this.tbPath);
            this.Name = "ViewInstallPath";
            this.Size = new System.Drawing.Size(527, 141);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbPath;
        private System.Windows.Forms.Button bBrowse;
        private System.Windows.Forms.Label label2;
    }
}
