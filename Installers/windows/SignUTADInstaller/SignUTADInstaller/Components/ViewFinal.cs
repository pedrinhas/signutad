﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SignUTADInstaller.Components
{
    public partial class ViewFinal : View
    {
        public ViewFinal()
        {
            InitializeComponent();

            this.CanGoBack = false;
        }
        public ViewFinal(Action a) : this()
        {
            switch (a)
            {
                case Action.Update: lText.Text = Constantes.Setup.FinalTextUpdate;
                    break;
                case Action.Uninstall: lText.Text = Constantes.Setup.FinalTextUninstall;
                    break;
                case Action.Install: lText.Text = Constantes.Setup.FinalTextInstall;
                    break;
            }
        }
    }
}
