﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace SignUTADInstaller.Components
{
    public partial class ViewWorking : View
    {
        private object o = 0;

        public event WorkFinishedDelegate WorkDone;

        public Action Action { get; private set; }
        public ViewWorking()
        {
            InitializeComponent();
        }
        public ViewWorking(Action a):this()
        {
            Action = a;

            switch (a)
            {
                case Action.Update:
                    lText.Text = Constantes.Setup.WorkingUpdate;
                    break;
                case Action.Uninstall:
                    lText.Text = Constantes.Setup.WorkingUninstall;
                    break;
                case Action.Install:
                    lText.Text = Constantes.Setup.WorkingInstall;
                    break;
                default:
                    break;
            }
        }
        [Obsolete("Mistérios", true)]
        public void AdvanceProgress(int adv)
        {
            SetProgress(pbProgress.Value + adv);
        }
        public void ResetProgress()
        {
            SetProgress(0);
        }
        public void SetProgress(int prog)
        {
            Thread t = new Thread(() =>
            {
                if (this.InvokeRequired)
                {
                    lock (o)
                    {
                        this.Invoke(new System.Action(delegate()
                        {
                            if (prog > pbProgress.Maximum) prog = pbProgress.Maximum;
                            pbProgress.Value = prog;
                            float percent = (int)((Convert.ToSingle(pbProgress.Value) / Convert.ToSingle(pbProgress.Maximum)) * 100);

                            lProgress.Text = string.Format("{0}%", percent);

                            if (pbProgress.Value >= pbProgress.Maximum && WorkDone != null) WorkDone();
                        }));
                    }
                }
                else
                {
                    lock (o)
                    {
                        if (prog > pbProgress.Maximum) prog = pbProgress.Maximum;
                        pbProgress.Value = prog;
                        float percent = (int)((Convert.ToSingle(pbProgress.Value) / Convert.ToSingle(pbProgress.Maximum)) * 100);

                        lProgress.Text = string.Format("{0}%", percent);

                        if (pbProgress.Value >= pbProgress.Maximum && WorkDone != null) WorkDone();
                    }
                }
            });

            t.Start();
        }
        public void SetProgressMax(int max)
        {
            Thread t = new Thread(() =>
            {
                if (this.InvokeRequired)
                {
                    this.Invoke(new System.Action(delegate()
                    {
                        lock (o)
                        {
                            pbProgress.Maximum = max;
                        }
                    }));
                }
                else
                {
                    lock (o)
                    {
                        pbProgress.Maximum = max;
                    }
                }
            });

            t.Start();
        }
    }
}
