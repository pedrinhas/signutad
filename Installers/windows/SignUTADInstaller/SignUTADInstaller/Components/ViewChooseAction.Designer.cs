﻿namespace SignUTADInstaller.Components
{
    partial class ViewChooseAction
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.rbUpdate = new System.Windows.Forms.RadioButton();
            this.rbUninstall = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(16, 16);
            this.label2.Margin = new System.Windows.Forms.Padding(16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(495, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "A aplicação já se encontra instalada. Escolha se pretende atualizar a aplicação o" +
    "u desinstalar.";
            // 
            // rbUpdate
            // 
            this.rbUpdate.AutoSize = true;
            this.rbUpdate.Checked = true;
            this.rbUpdate.Location = new System.Drawing.Point(19, 50);
            this.rbUpdate.Name = "rbUpdate";
            this.rbUpdate.Size = new System.Drawing.Size(65, 17);
            this.rbUpdate.TabIndex = 3;
            this.rbUpdate.TabStop = true;
            this.rbUpdate.Text = "Atualizar";
            this.rbUpdate.UseVisualStyleBackColor = true;
            // 
            // rbUninstall
            // 
            this.rbUninstall.AutoSize = true;
            this.rbUninstall.Location = new System.Drawing.Point(19, 73);
            this.rbUninstall.Name = "rbUninstall";
            this.rbUninstall.Size = new System.Drawing.Size(77, 17);
            this.rbUninstall.TabIndex = 4;
            this.rbUninstall.Text = "Desinstalar";
            this.rbUninstall.UseVisualStyleBackColor = true;
            // 
            // ViewChooseAction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.rbUninstall);
            this.Controls.Add(this.rbUpdate);
            this.Controls.Add(this.label2);
            this.Name = "ViewChooseAction";
            this.Size = new System.Drawing.Size(527, 141);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbUpdate;
        private System.Windows.Forms.RadioButton rbUninstall;
    }
}
