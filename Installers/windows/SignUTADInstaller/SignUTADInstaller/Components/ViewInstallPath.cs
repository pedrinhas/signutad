﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SignUTADInstaller.Components
{
    public partial class ViewInstallPath : View
    {
        public string InstallPath { get; set; }

        public ViewInstallPath()
        {
            InitializeComponent();

            tbPath.Text = InstallPath = Constantes.Setup.DefaultInstallPath;
        }

        private void bBrowse_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            fbd.RootFolder = Environment.SpecialFolder.MyComputer;

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                tbPath.Text = this.InstallPath = string.Format("{0}\\{1}", fbd.SelectedPath, Constantes.Setup.InstallSubFolders);
            }
        }

        public bool CanAdvance
        {
            get
            {
                return !string.IsNullOrWhiteSpace(InstallPath);
            }
        }
    }
}
