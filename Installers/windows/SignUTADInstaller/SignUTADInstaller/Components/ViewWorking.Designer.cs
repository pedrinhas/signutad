﻿namespace SignUTADInstaller.Components
{
    partial class ViewWorking
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lText = new System.Windows.Forms.Label();
            this.pbProgress = new System.Windows.Forms.ProgressBar();
            this.lProgress = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lText
            // 
            this.lText.Location = new System.Drawing.Point(16, 16);
            this.lText.Margin = new System.Windows.Forms.Padding(16);
            this.lText.Name = "lText";
            this.lText.Size = new System.Drawing.Size(495, 61);
            this.lText.TabIndex = 1;
            this.lText.Text = "Está prestes a instalar a aplicação de assinatura digital da Universidade de Trás" +
    "-os-Montes e Alto Douro.\r\n\r\nPara continuar, clique em \"Seguinte\".";
            // 
            // pbProgress
            // 
            this.pbProgress.Location = new System.Drawing.Point(19, 96);
            this.pbProgress.Name = "pbProgress";
            this.pbProgress.Size = new System.Drawing.Size(451, 23);
            this.pbProgress.TabIndex = 2;
            // 
            // lProgress
            // 
            this.lProgress.AutoSize = true;
            this.lProgress.Location = new System.Drawing.Point(476, 101);
            this.lProgress.Margin = new System.Windows.Forms.Padding(3, 0, 16, 0);
            this.lProgress.Name = "lProgress";
            this.lProgress.Size = new System.Drawing.Size(35, 13);
            this.lProgress.TabIndex = 3;
            this.lProgress.Text = "label1";
            // 
            // ViewWorking
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lProgress);
            this.Controls.Add(this.pbProgress);
            this.Controls.Add(this.lText);
            this.Name = "ViewWorking";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lText;
        private System.Windows.Forms.ProgressBar pbProgress;
        private System.Windows.Forms.Label lProgress;
    }
}
