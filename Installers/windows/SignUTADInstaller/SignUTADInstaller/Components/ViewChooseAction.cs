﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SignUTADInstaller.Components
{
    public partial class ViewChooseAction : View
    {
        public Action Action
        {
            get
            {
                if (rbUpdate.Checked) return Components.Action.Update;
                else if (rbUninstall.Checked) return Components.Action.Uninstall;
                else return Components.Action.Update;
            }
        }
        public ViewChooseAction()
        {
            InitializeComponent();
        }

        public bool CanAdvance
        {
            get { return rbUninstall.Checked || rbUpdate.Checked; }
        }
    }
}
