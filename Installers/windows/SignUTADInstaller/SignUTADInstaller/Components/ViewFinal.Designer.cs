﻿namespace SignUTADInstaller.Components
{
    partial class ViewFinal
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lText = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lText
            // 
            this.lText.Location = new System.Drawing.Point(16, 16);
            this.lText.Margin = new System.Windows.Forms.Padding(16);
            this.lText.Name = "lText";
            this.lText.Size = new System.Drawing.Size(495, 109);
            this.lText.TabIndex = 1;
            this.lText.Text = "Está prestes a instalar a aplicação de assinatura digital da Universidade de Trás" +
    "-os-Montes e Alto Douro.\r\n\r\nPara continuar, clique em \"Seguinte\".";
            // 
            // ViewFinal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lText);
            this.Name = "ViewFinal";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lText;
    }
}
