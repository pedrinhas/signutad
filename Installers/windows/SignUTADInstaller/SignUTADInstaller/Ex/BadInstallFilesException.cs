﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignUTADInstaller.Ex
{
    public class BadInstallFilesException : Exception
    {
        public BadInstallFilesException()
        {
        }
        public BadInstallFilesException(string message) : base(message)
        {
        }
        public BadInstallFilesException(string message, Exception inner) : base(message, inner)
        {
        }
        public BadInstallFilesException(Exception inner) : base("", inner) { }
    }
}
