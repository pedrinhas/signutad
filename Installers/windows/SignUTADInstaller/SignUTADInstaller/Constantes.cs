﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SignUTADInstaller
{
    public static class Constantes
    {
        public static class UI
        {
            public static string MessageExitConfirmation { get { return "Tem a certeza que pretende cancelar a instalação?"; } }
            public static string MessageExitConfirmationCaption { get { return "Sair?"; } }
            public static string ButtonNextText { get { return "Seguinte"; } }
            public static string ButtonUpdateText { get { return "Atualizar"; } }
            public static string ButtonInstallText { get { return "Instalar"; } }
            public static string ButtonUninstallText { get { return "Desinstalar"; } }
            public static string ButtonNextFinalText { get { return "Fechar"; } }
        }
        public static class Setup
        {
            static bool is64BitProcess = (IntPtr.Size == 8);
            static bool is64BitOperatingSystem = is64BitProcess || InternalCheckIsWow64;

            [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool IsWow64Process(
                [In] IntPtr hProcess,
                [Out] out bool wow64Process
            );

            private static bool InternalCheckIsWow64
            {
                get
                {
                    if ((Environment.OSVersion.Version.Major == 5 && Environment.OSVersion.Version.Minor >= 1) ||
                        Environment.OSVersion.Version.Major >= 6)
                    {
                        using (Process p = Process.GetCurrentProcess())
                        {
                            bool retVal;
                            if (!IsWow64Process(p.Handle, out retVal))
                            {
                                return false;
                            }
                            return retVal;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            public static string DefaultInstallPath
            {
                get
                {
                    string progFiles = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

                    return string.Format("{0}\\{1}", progFiles, InstallSubFolders);
                }
            }
            public static string InstallSubFolders { get { return "UTAD\\Assinatura"; } }

            public static string FinalTextInstall
            {
                get
                {
                    return "A aplicação de assinatura digital da Universidade de Trás-os-Montes e Alto Douro foi instalada com sucesso.\r\nJá poderá assinar documentos digitalmente, usando o Cartão de Cidadão, no GesDoc.\r\n\r\nPara sair, clique em \"Fechar\".";
                }
            }
            public static string FinalTextUninstall
            {
                get
                {
                    return "A aplicação de assinatura digital da Universidade de Trás-os-Montes e Alto Douro foi desinstalada com sucesso.\r\n\r\nPara sair, clique em \"Fechar\".";
                }
            }
            public static string FinalTextUpdate
            {
                get
                {
                    return "A aplicação de assinatura digital da Universidade de Trás-os-Montes e Alto Douro foi atualizada com sucesso.\r\nPode continuar a assinar documentos digitalmente, usando o Cartão de Cidadão, no GesDoc.\r\n\r\nPara sair, clique em \"Fechar\".";
                }
            }

            public static string WorkingInstall { get { return "A instalar a aplicação. Por favor, aguarde."; } }
            public static string WorkingUninstall { get { return "A desinstalar a aplicação. Por favor, aguarde."; } }
            public static string WorkingUpdate { get { return "A atualizar a aplicação. Por favor, aguarde."; } }
            /// <summary>
            /// Pasta com atalhos para a versão do java a ser usada. Usado no registo porque para atualizar o caminho do Java é preciso instalar a aplicação de novo.
            /// </summary>
            public static string JavaPathSymLink { get { return @"C:\ProgramData\Oracle\Java\javapath"; } }

            public static string JarName { get { return Properties.Resources.SignUTADDesktopName; } }
            public static string UpdaterName { get { return Properties.Resources.UpdaterName; } }

            public static string JRELink { get { return "https://java.com/pt/download/"; } }

            public static class Errors
            {
                public static string MessageBoxErrorCaption { get { return "Erro"; } }

                public static string Generic { get { return "Ocorreu um erro inesperado. Por favor contacte o apoio ao utilizador do GesDoc."; } }

                public static string BadInstallFiles { get { return "Os ficheiros de instalação não estão corretos. Por favor volte a fazer download do instalador."; } }
                public static string NoJRE { get { return string.Format("Ainda não tem o Java instalado. Deseja abrir a página de download?{0}Deverá realizar a instalação da Aplicação de Assinatura quando o Java estiver instalado.", Environment.NewLine); } }
            }
        }
    }
}
