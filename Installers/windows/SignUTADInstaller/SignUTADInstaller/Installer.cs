﻿using Microsoft.Win32;
using SignUTADInstaller.Components;
using SignUTADInstaller.Ex;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SignUTADInstaller
{
    public partial class Installer : Form
    {
        public SignUTADInstaller.Components.Action ActionPerformed { get; set; }
        bool AskConfirmation = true;

        public Installer()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            this.ActionPerformed = Components.Action.Install;

            mvMain.AddView(new ViewIntro());
            if (CheckInstalled())
            {
                mvMain.AddView(new ViewChooseAction());
            }
            else
            {
#if VDI
                mvMain.AddView(new ViewInstallPath());
#endif
                mvMain.AddView(new ViewConfirmInstall());
                mvMain.AddView(new ViewWorking(this.ActionPerformed));
                mvMain.AddView(new ViewFinal(this.ActionPerformed));
            }

            // Se o sistema não possuir uma instalação do Java, aborta tudo e apresenta uma mensagem de erro
            if (!CheckJava())
            {
                if (MessageBox.Show(Constantes.Setup.Errors.NoJRE, "Java", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    System.Diagnostics.Process.Start(Constantes.Setup.JRELink);
                this.AskConfirmation = false;
                Application.Exit();
            }
        }



        private void mvMain_OnViewChanging(Components.View activeView)
        {
            if (activeView is ViewChooseAction)
            {
                var v = activeView as ViewChooseAction;

                switch (v.Action)
                {
                    case SignUTADInstaller.Components.Action.Update:
                        mvMain.Views.RemoveAll(x => x is ViewConfirmUninstall); //VIEW UNINSTALL
                        mvMain.Views.RemoveAll(x => x is ViewWorking);
                        mvMain.AddView(new Components.ViewConfirmUpdate()); //VIEW UPDATE
                        mvMain.AddView(new Components.ViewWorking(v.Action)); //VIEW UPDATE
                        break;
                    case SignUTADInstaller.Components.Action.Uninstall:
                        mvMain.Views.RemoveAll(x => x is ViewConfirmUpdate); //VIEW UPDATE
                        mvMain.Views.RemoveAll(x => x is ViewWorking);
                        mvMain.AddView(new Components.ViewConfirmUninstall()); //VIEW UNINSTALL
                        mvMain.AddView(new Components.ViewWorking(v.Action)); //VIEW UPDATE
                        break;
                    default:
                        break;
                }

                this.ActionPerformed = v.Action;
                mvMain.Views.RemoveAll(x => x is ViewFinal);
                mvMain.AddView(new ViewFinal(v.Action));
            }
            else if (activeView is ViewConfirmInstall || activeView is ViewConfirmUninstall || activeView is ViewConfirmUpdate)
            {
                bNext.Text = Constantes.UI.ButtonNextText;
            }
        }

        private void mvMain_OnViewChanged(Components.View activeView)
        {
            bNext.Enabled = activeView.CanAdvance;
            bPrev.Enabled = activeView.CanGoBack;

            if (activeView is ViewChooseAction) return;

            if (mvMain.IsLastView) bNext.Text = Constantes.UI.ButtonNextFinalText;
            else bNext.Text = Constantes.UI.ButtonNextText;

            if (activeView is ViewWorking)
            {
                var v = activeView as ViewWorking;
                v.WorkDone += v_WorkDone;
                switch (v.Action)
                {
                    case SignUTADInstaller.Components.Action.Update:
                        Uninstall(v);
                        Install(v);
                        break;
                    case SignUTADInstaller.Components.Action.Uninstall:
                        Uninstall(v);
                        break;
                    case SignUTADInstaller.Components.Action.Install:
                        Install(v);
                        break;
                    default:
                        break;
                }
            }
            else if (activeView is ViewConfirmInstall)
            {
                bNext.Text = Constantes.UI.ButtonInstallText;
            }
            else if (activeView is ViewConfirmUninstall)
            {
                bNext.Text = Constantes.UI.ButtonUninstallText;
            }
            else if (activeView is ViewConfirmUpdate)
            {
                bNext.Text = Constantes.UI.ButtonUpdateText;
            }
        }

        void v_WorkDone()
        {
            mvMain.Next();
        }

        private void bPrev_Click(object sender, EventArgs e)
        {
            mvMain.Prev();


        }

        private void bNext_Click(object sender, EventArgs e)
        {
            if (bNext.Text == Constantes.UI.ButtonNextFinalText) Application.Exit();
            mvMain.Next();

            //skip
            //if (mvMain.ActiveView is ViewInstallPath) mvMain.Next();
        }

        private string GetInstallationPath()
        {
            Microsoft.Win32.RegistryKey keyInstall = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE").OpenSubKey("UTAD");
            if (keyInstall == null) return "";
            var keyPath = keyInstall.OpenSubKey("Assinatura");
            if (keyPath == null) return "";
            var path = keyPath.GetValue("Path").ToString();

            if (!(new DirectoryInfo(path)).Exists)
            {
                Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE", true).OpenSubKey("UTAD", true).DeleteSubKeyTree("Assinatura");
                return "";
            }

            return path;
        }

        /// <summary>
        /// Verifica se a aplicação já está instalada. Tem uma chave no registo
        /// </summary>
        /// <returns></returns>
        private bool CheckInstalled()
        {
            return !string.IsNullOrEmpty(GetInstallationPath());
        }
        /// <summary>
        /// Verifica, pela variável de ambiente Path, se o JRE está instalado
        /// </summary>
        /// <returns></returns>
        private bool CheckJava()
        {
            //VERIFICAR SE TEM JRE
            return Environment.GetEnvironmentVariables().Contains("JAVA_HOME") || Environment.GetEnvironmentVariable("Path").ToLower().Contains("java");
        }

        static string GetJavaInstallationPath()
        {
            string environmentPath = Environment.GetEnvironmentVariable("JAVA_HOME");
            if (!string.IsNullOrEmpty(environmentPath))
            {
                return environmentPath;
            }

            const string JAVA_KEY = "SOFTWARE\\JavaSoft\\Java Runtime Environment\\";

            var localKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry32);
            using (var rk = localKey.OpenSubKey(JAVA_KEY))
            {
                if (rk != null)
                {
                    string currentVersion = rk.GetValue("CurrentVersion").ToString();
                    using (var key = rk.OpenSubKey(currentVersion))
                    {
                        return key.GetValue("JavaHome").ToString();
                    }
                }
            }

            localKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry64);
            using (var rk = localKey.OpenSubKey(JAVA_KEY))
            {
                if (rk != null)
                {
                    string currentVersion = rk.GetValue("CurrentVersion").ToString();
                    using (var key = rk.OpenSubKey(currentVersion))
                    {
                        return key.GetValue("JavaHome").ToString();
                    }
                }
            }

            return null;
        }

        private void Install(ViewWorking v)
        {
            // O programa só chega a este ponto se o sistema possuir uma instalação válida do Java
            string p = "";

            try
            {
                v.SetProgress(0);

                string path = "";

                if (mvMain.Views.OfType<ViewInstallPath>().Count() == 0) path = GetInstallationPath();
                else path = mvMain.Views.OfType<ViewInstallPath>().First().InstallPath;

                if (string.IsNullOrWhiteSpace(path)) path = Constantes.Setup.DefaultInstallPath;

                p = path;

                #region Copiar ficheiros

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                string mainUrl = "https://gesdoc.utad.pt/Installers/Assinatura/Assinatura%20Digital%20UTAD.jar";
                string updaterUrl = "https://gesdoc.utad.pt/Installers/Assinatura/updater.jar";

                string mainPath = string.Format("{0}\\{1}", path.Trim('\\'), Properties.Resources.SignUTADDesktopName);
                string updaterPath = string.Format("{0}\\{1}", path.Trim('\\'), Properties.Resources.UpdaterName);

                bNext.Enabled = false;
                bPrev.Enabled = false;

                Thread t = new Thread(() =>
                {
                    Tools.Net.DownloadBytes(mainUrl, mainPath);
                    Tools.Net.DownloadBytes(updaterUrl, updaterPath);


                });
                t.Start();
                t.Join();
                
                #endregion
                #region Criar chave utadsign://

                string jarPath = string.Format("{0}\\{1}", path.Trim('\\'), Constantes.Setup.JarName);

                RegistryKey key = Registry.ClassesRoot.CreateSubKey("utadsign");

                key.SetValue(null, "URL:Endereço de assinatura de documentos na UTAD");
                key.SetValue("URL Protocol", string.Empty);

                Registry.ClassesRoot.CreateSubKey("utadsign" + "\\Shell");
                Registry.ClassesRoot.CreateSubKey("utadsign" + "\\Shell\\open");
                key = Registry.ClassesRoot.CreateSubKey("utadsign" + "\\Shell\\open\\command");

                // Obtém o caminho da pasta onde se encontra o javaw.exe
                string javawFolderPath = string.Format("{0}\\bin", GetJavaInstallationPath());

                // Com o caminho anteriormente obtido, constrói o comando
                // mvale - Este caminho foi alterado, em 26/09/2019, para a aplicação obter o caminho do Java de uma maneira diferente
                // string javawCommand = string.Format("\"{0}\\javaw.exe\" -jar \"{1}\" \"%1\"", javawFolderPath/*Constantes.Setup.JavaPathSymLink*/, jarPath);
                string javawCommand = string.Format("cmd /c START /MIN \"Title\" cmd /c \"java -jar \"{0}\" \"%1\"\"", jarPath);

                // Define a string anterior no registo do Windows
                key.SetValue(null, javawCommand);
                
                #endregion
                #region Criar chave instalação

                Microsoft.Win32.RegistryKey keyInstall = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE", true).CreateSubKey("UTAD");
                var keyInstall1 = keyInstall.CreateSubKey("Assinatura");
                keyInstall1.SetValue("Path", path);

                #endregion

                v.SetProgress(100);
            }
            catch (BadInstallFilesException ex)
            {
                HandleError(Constantes.Setup.Errors.BadInstallFiles, true, p);
            }
            catch (System.IO.IOException ex)
            {
                HandleError(string.Format("{1}{0}{0}{2}{0}{3}", Environment.NewLine, Constantes.Setup.Errors.Generic, ex.Message, ex.StackTrace), true, p);
            }
        }



        private void Uninstall(ViewWorking v, string badInstallPath = null)
        {
            if (v != null) v.SetProgress(0);

            string installPath = badInstallPath ?? GetInstallationPath();

            if (string.IsNullOrWhiteSpace(installPath)) return;

            #region Ficheiros

            DirectoryInfo installDir = new DirectoryInfo(installPath);
            if (installDir.Exists)
            {
                if (installDir.EnumerateFiles().Select(x => x.Name).Contains(Constantes.Setup.JarName))
                {
                    File.Delete(installDir.GetFiles().Single(x => x.Name == Constantes.Setup.JarName).FullName);
                }
                if (installDir.EnumerateFiles().Select(x => x.Name).Contains(Constantes.Setup.UpdaterName))
                {
                    File.Delete(installDir.GetFiles().Single(x => x.Name == Constantes.Setup.UpdaterName).FullName);
                }
                if (installDir.EnumerateDirectories().Select(x => x.Name).Contains("lib"))
                {
                    Directory.Delete(installDir.EnumerateDirectories().Single(x => x.Name == "lib").FullName, true);
                }
            }

            #endregion
            if (v != null) v.SetProgress(33);
            #region Protocolo

            if (Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("utadsign") != null)
                Microsoft.Win32.Registry.ClassesRoot.DeleteSubKeyTree("utadsign");

            #endregion
            if (v != null) v.SetProgress(67);
            #region Chave instalação

            Microsoft.Win32.RegistryKey keyInstall = null;
            if((keyInstall = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE", true).OpenSubKey("UTAD", true)) != null)
            {
                if(keyInstall.OpenSubKey("Assinatura") != null)
                keyInstall.DeleteSubKeyTree("Assinatura");
            }

            #endregion
            if (v != null) v.SetProgress(100);
        }

        public void HandleError(string message, bool uninstall = false, string badInstallPath = null)
        {
            MessageBox.Show(message, Constantes.Setup.Errors.MessageBoxErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);

            if (uninstall) 
            {
                Uninstall(null, badInstallPath);
            }
            this.AskConfirmation = false;

            Application.Exit();
        }

        #region exit?
        private void bCancelar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (bNext.Text == Constantes.UI.ButtonNextFinalText) return;
            if (!AskConfirmation)
            {
                e.Cancel = false;
                return;
            }

            e.Cancel = MessageBox.Show(Constantes.UI.MessageExitConfirmation, Constantes.UI.MessageExitConfirmationCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No;
        }
        #endregion
    }
}
