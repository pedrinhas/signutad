﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Threading;
using SignUTADInstaller.Components;

namespace SignUTADInstaller
{
    public static class Tools
    {
        public static class Net
        {
            private static object o = 0;

            public static void DownloadBytes(string url, string location)
            {
                try
                {
                    using (var fs = File.OpenWrite(location))
                    {
                        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                        var res = req.GetResponse();

                        var resStream = res.GetResponseStream();

                        resStream.CopyTo(fs);
                    }
                }
                catch (Exception ex)
                {
                    SignUTADInstaller.Ex.BadInstallFilesException ex2 = new Ex.BadInstallFilesException(ex);
                    throw ex2;
                }
            }

            public static int GetContentLength(string url)
            {
                var req = (HttpWebRequest)HttpWebRequest.Create(url);
                req.Method = "HEAD";

                using (var res = req.GetResponse() as HttpWebResponse)
                {
                    var statusCode = res.StatusCode;

                    if (statusCode == HttpStatusCode.OK && res.Headers.AllKeys.Contains("Content-Length"))
                    {
                        return int.Parse(res.Headers["Content-Length"]);
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }
    }
}
