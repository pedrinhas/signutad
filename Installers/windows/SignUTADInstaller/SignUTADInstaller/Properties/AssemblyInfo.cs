﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Assinatura Digital UTAD")]
[assembly: AssemblyDescription("Software de assinatura da Universidade de Trás-os-Montes e Alto Douro")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Universidade de Trás-os-Montes e Alto Douro")]
[assembly: AssemblyProduct("Assinatura Digital UTAD")]
[assembly: AssemblyCopyright("Copyright ©  2016")]
[assembly: AssemblyTrademark("UTAD")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("3ff0903a-0b3e-4678-938b-8b05cf5bca92")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.7.4.0")]
[assembly: AssemblyFileVersion("1.5.0")]
